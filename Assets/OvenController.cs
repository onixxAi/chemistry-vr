﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class OvenController : MonoBehaviour
{
    public bool capOpened = false;
    public bool vacuum;
    private string rightAnswer = "CuO";
    private string[] rights = new string[] { "La", "Al", "C", "Ca" };
    public bool isRight = true;
    public bool haveBeen;
    public bool isUsed = false;
    private float timer = 1f;
    private int timing = 20;
    private float time = 0;
    public bool rewind = false;
    public bool coock = false;
    public float temp = 21;
    public TextMeshPro textTemp;
    public TextMeshPro textTime;
    public bool HadMainAnswer = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        capOpened = GameObject.Find("CAP").GetComponent<openContainer>().isOpen;
        if(coock)
        {
            timer -= Time.deltaTime;
            if (time < 1)
            {

                if (rewind)
                {
                    time += Time.deltaTime / 10;
                }
                else
                {
                    time += Time.deltaTime / 10;
                }


            }

            if (rewind)
            {
                temp = Mathf.Lerp(1250, 21, time);
                temp = (int)Mathf.Round(temp);
            }
            else
            {
                temp = Mathf.Lerp(21, 1250, time);
                temp = (int)Mathf.Round(temp);
            }

            if (timer < 0)
            {
                if (timing > 0)
                {
                    timing -= 1;
                    timer = 1f;
                }
            }
            textTemp.text = temp.ToString() + "°C";
            textTime.text = timing.ToString() + " сек.";
            if(rewind)
            {
                Debug.Log("Checking");
                Debug.Log(temp);
                if (temp < 22 && time > 0)
                {
                    time = 0;
                    timing = 0;
                    textTime.text = timing.ToString() + " сек.";
                    temp = 21f;
                    Debug.Log("Yes there");
                    textTemp.text = temp.ToString() + "°C";
                    rewind = false;
                    Debug.Log("Yes Here");
                    coock = false;
                    isUsed = true;
                    if (isRight&& haveBeen && HadMainAnswer)
                    {
                        Debug.Log("RIGHT!");
                        GameObject.FindObjectOfType<GoalManager>().i += 1;
                        
                    }
                    else
                    {
                        Debug.Log("ERR!");
                        GameObject.Find("BackEnd").GetComponent<PlayerEventsStorage>().AddErrs();
                        //TODO: SUBTITLES
                    }


                }
            }
            else
            {
                if (temp > 1249 && time > 0)
                {
                    time = 0;
                    temp = 1250;
                    textTemp.text = temp.ToString() + "°C";
                    rewind = true;
                    
                    
                }

            }

        }
        
    }
    public void OpenCap()
    {
        capOpened = !capOpened;
        vacuum = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<LiCube>())
        {
            Debug.Log("Name is " + other.name);
            haveBeen = true;
            bool yes = false;
            foreach(string s in rights)
            {
                if(other.name == s)
                {
                    yes = true;
                    break;
                }
            }
            if(!yes)
            {
                if (other.name != rightAnswer) isRight = false;
                else HadMainAnswer = true;
            }
            Destroy(other.GetComponent<GameObject>());
        }
        
        
    }

    public void Clear()
    {
        vacuum = false;
        coock = false;
        rewind = false;
        isRight = true;
        haveBeen = false;
        isUsed = false;
        timing = 20;
        temp = 21;
        time = 0;
        timer = 1f;
        textTemp.text = temp.ToString() + "°C";
        textTime.text = timing.ToString() + " сек.";

    }
}
