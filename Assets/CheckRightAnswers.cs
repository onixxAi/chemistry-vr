﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckRightAnswers : MonoBehaviour
{
    public int rightAnswers;
    public GoalManager goalManager;
    private void Start()
    {
        rightAnswers = 0;
        goalManager = GameObject.FindObjectOfType<GoalManager>();
    }
    private void Update()
    {
        if (rightAnswers == 3)
        {

            goalManager.i += 2;
            gameObject.SetActive(false);
        }
    }
}
