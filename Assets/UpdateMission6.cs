﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class UpdateMission6 : MonoBehaviour
{
    public GameObject respawnCube;
    public Transform spawnPoint;
    void Update()
    {
        if (GetComponent<Interactable>().isHovering == true)
        {
            if (SteamVR_Actions.default_GrabPinch.GetStateDown(SteamVR_Input_Sources.Any))
            {
                Debug.Log("Pressed");
                GameObject.FindObjectOfType<AlkalineCooking>().cubeWeight = 0;
                Destroy(GameObject.Find("Контенер c литием(Clone)"));
                Instantiate(respawnCube, spawnPoint);
            }
        }
    }
}
