﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PadlaControlling : MonoBehaviour
{
    private Material endMaterial;
    private Material startMaterial;
    public bool metil;
    public bool lakmus;
    public bool fenol;
    public GameObject[] trigger;
    private void Update()
    {
        if (metil||lakmus||fenol)
        {
            trigger[0].SetActive(true);
            trigger[1].SetActive(true);
            
            //trigger.SetActive(true);
        }
    }
    private void Start()
    {
        startMaterial = GetComponent<Renderer>().material;
    }
    public void ChangeMaterial(Material blobMaterial, float lerp)
    {
        
        Debug.Log("Material Changing");
        if (blobMaterial != endMaterial)
        {
            Debug.Log("lerp: "+lerp);
            endMaterial = blobMaterial;
            startMaterial.Lerp(startMaterial, endMaterial, lerp);
            lerp = 0;

        }
        lerp += lerp;
        GetComponent<Renderer>().material.Lerp(startMaterial, endMaterial, lerp);

    }
}
