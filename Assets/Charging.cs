﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Charging : MonoBehaviour
{
    public TextMeshPro procentText;
    public float procent;
    public GameObject chargingBar;
    public bool isCharging;
    public float time = 0;
    public GameObject uiCanvas;
    public bool temperature;
    public bool tempertureOverride;
    private void Update()
    {
        
        if(isCharging)
        {
            if (time < 1)
            {
               
                 if(tempertureOverride)
                {
                    time += Time.deltaTime / 50;
                }
                 else
                {
                    time += Time.deltaTime / 10;
                }
                    
                
            }
            
            if (tempertureOverride)
            {
                procent = Mathf.Lerp(100, 200, time);
                procent = (int)Mathf.Round(procent);
            }
            else
            {
                procent = Mathf.Lerp(1, 100, time);
                procent = (int)Mathf.Round(procent);
            }
            if (temperature || tempertureOverride)
            {
                procentText.text = procent.ToString() + "°C";
            }
            else
            {
                procentText.text = procent.ToString() + "%";

            }
            if (chargingBar)
            {
                chargingBar.transform.localPosition = new Vector3(chargingBar.transform.localPosition.x, Mathf.Lerp(-2f, 0, time), chargingBar.transform.localPosition.z);
                chargingBar.transform.localScale = new Vector3(chargingBar.transform.localScale.x, Mathf.Lerp(.2f, 1, time), chargingBar.transform.localScale.z);
                chargingBar.GetComponent<SpriteRenderer>().color = new Color(Mathf.Lerp(1, 0, time), Mathf.Lerp(0, 1, time), 0, 1);
            }
           
        }
        if (procent == 99 && time >0)
        {
            procent = 100;
            if (temperature || tempertureOverride)
            {
                procentText.text = procent.ToString() + "°C";
            }
            else
            {
                procentText.text = procent.ToString() + "%";

            }
            isCharging = false;
            time = 0;
            if (!temperature)
            {
                GameObject.FindObjectOfType<GoalManager>().i += 1;
            }
           
            if (uiCanvas)
            {
                Destroy(uiCanvas);
            }
        }
    }



}
