﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class VRKeyboard : MonoBehaviour
{
    public GoalManager goalManager;
    public GameObject errorSubtitles;
    public string rightAnswer;
    public TextMeshPro[] input;
    public AnswerControl[] fields;
    public string writeAnswer;
    public bool findValidGazes;
    [HideInInspector]
    public PlayerEventsStorage eventHandler;
    public string[] badAnswers = new string[] { "H2S", "Cl2", "C3H8", "PH3" };
    public string[] rightAnswers = new string[] 
    {
        "Ca(OH)2", "CaO"
    };
    public int counterOfPushedButtons = 0;
    public bool containsRightAnswer = false;
    public int counterOfAnswers = 0;
    

    public int curLenghtOfAswer = 0;
    public int curPosInTextBlockArray = 0;
    public int lengthOfAnswer = 2;
    
    private void Start()
    {
        eventHandler = GameObject.Find("BackEnd").GetComponent<PlayerEventsStorage>();
    }

    public void CheckAnswer(string answer)
    {
        //if (findLiquid)
        //{
        //    if (answer == "CO2")
        //    {
        //        writeAnswer = "";
        //        print("completed");

        //    }
            
        //}
            

        if(answer == "Enter")
        {
            
            if (findValidGazes)
            {
                //print(rightAnswer.Contains(writeAnswer));
                

            }
            if (rightAnswer.Length == writeAnswer.Length)
            {
                if (rightAnswer == writeAnswer)
                {
                    writeAnswer = "";
                    print("completed");
                    //Destroy(gameObject);
                    goalManager.i += 1;
                }
                else
                {
                    if (errorSubtitles)
                    {

                        Instantiate(errorSubtitles, GameObject.Find("SubtitlesSpawnPoint").transform);
                    }
                    eventHandler.AddErrs();
                }
                    
            }

        }
        if (answer == "Backspace")
        {
            
            writeAnswer = writeAnswer.Remove(writeAnswer.Length-1);
        }
        else if (writeAnswer.Length< rightAnswer.Length)
        {
                writeAnswer += answer;
        }
        if (!findValidGazes)
        {
            input[0].GetComponent<TextMeshPro>().text = writeAnswer;
        }
        
    }
    public void ClearAll()
    {
        foreach (TextMeshPro curtext in input)
        {
            curtext.text = "";
        }
        foreach (AnswerControl text in fields)
        {
            text.Clear();
        }

    }

    public void KeyboardManager(string value)
    {
       
        if(fields[curPosInTextBlockArray].filled)
        {
            if(curPosInTextBlockArray < fields.Length-1) curPosInTextBlockArray++;
            
        }
         Debug.Log("value: " + value);
        if (value == "Backspace")
        {
            Debug.Log("inBack");
            if (fields[curPosInTextBlockArray].readyToGoDown)
            {
                Debug.Log("redyGoDow"+ curPosInTextBlockArray);
                
                if (curPosInTextBlockArray > 0)
                {
                    Debug.Log("DeleteBack");
                    curPosInTextBlockArray-=1;
                }
                
            }

        }
        
        fields[curPosInTextBlockArray].WriteSymbol(value);

    }

    public void GetDataFromNumBoard(string value)
    {
        if (value == "Backspace")
        {
            if (input[curPosInTextBlockArray].text.Length>0)
            {
                input[curPosInTextBlockArray].text = input[curPosInTextBlockArray].text.Remove(input[curPosInTextBlockArray].text.Length - 1, 1);
                counterOfAnswers--;
            }
            else if(curPosInTextBlockArray>0)
            {
                curPosInTextBlockArray--;
                input[curPosInTextBlockArray].text = input[curPosInTextBlockArray].text.Remove(input[curPosInTextBlockArray].text.Length - 1, 1);
                counterOfAnswers--;
            }
                
               
            
            
        }
        else if (curPosInTextBlockArray < input.Length)
        {
            
            {
                if (input[curPosInTextBlockArray].text.Length < lengthOfAnswer)
                {
                    input[curPosInTextBlockArray].text += value;
                    counterOfAnswers++;
                }
                else
                {
                    curPosInTextBlockArray++;
                    
                    input[curPosInTextBlockArray].text += value;
                    counterOfAnswers++;
                }
            }
        }
    }


    public void GetButtonName(string name)
    {
      
        
        foreach(TextMeshPro curtext in input)
        {
            if(curtext.text == name)
            {
                if (name == "CO2") containsRightAnswer = false;
                curtext.text = "__";
                counterOfPushedButtons--;
                return;
            }
        }
        foreach (TextMeshPro curtext in input)
        {
            if(curtext.text == "__")
            {
                if (name == "CO2") containsRightAnswer = true;
                curtext.text = name;
                counterOfPushedButtons++;
                return;
            }
        }
    }
    public void ChackanswersWithRightAnswers()
    {
        string output = "";
        foreach (TextMeshPro text in input)
        {
            output += text.text;
        }
        Debug.Log("output: " + output);
        if (output.Contains(rightAnswer))
        {
            bool flag = true;
            foreach (string s in rightAnswers)
            {
                if (output.Contains(s))
                {
                    
                    flag = false;
                }
            }
            if (!flag)
            {
                print("Answer is right");
                //Destroy(gameObject);
                ClearAll();
                goalManager.i += 1;
            }
            else
            {
                Debug.Log("Wrong Answer");
                if (errorSubtitles)
                {

                    Instantiate(errorSubtitles, GameObject.Find("SubtitlesSpawnPoint").transform);
                }
                //TODO: add "wrong" subtitles
                eventHandler.AddErrs();
            }
        }
        else
        {
            Debug.Log("Wrong Answer");
            if (errorSubtitles)
            {

                Instantiate(errorSubtitles, GameObject.Find("SubtitlesSpawnPoint").transform);
            }
            //TODO: add "wrong" subtitles
            eventHandler.AddErrs();
        }
    }

    public void ChackanswersWithRightAnswersKeyboard()
    {
        string output = "";
        foreach (AnswerControl text in fields)
        {
            output += text.GetText();
        }
        Debug.Log("output: " + output);
        if (output.Contains(rightAnswer))
        {
            bool flag = false;
            foreach (string s in rightAnswers)
            {
                if (!output.Contains(s))
                {

                    flag = true;
                }
            }
            if (!flag)
            {
                print("Answer is right");
                //Destroy(gameObject);
                ClearAll();
                goalManager.i += 1;
            }
            else
            {
                Debug.Log("Wrong Answer");
                if (errorSubtitles)
                {

                    Instantiate(errorSubtitles, GameObject.Find("SubtitlesSpawnPoint").transform);
                }
                //TODO: add "wrong" subtitles
                eventHandler.AddErrs();
            }
        }
        else
        {
            Debug.Log("Wrong Answer");
            if (errorSubtitles)
            {

                Instantiate(errorSubtitles, GameObject.Find("SubtitlesSpawnPoint").transform);
            }
            //TODO: add "wrong" subtitles
            eventHandler.AddErrs();
        }
    }

    public void CheckAnswerLogic()
    {
        string output = "";
        foreach(TextMeshPro text in input)
        {
            output += text.text;
        }
        Debug.Log("output: " + output);
        if (output.Contains(rightAnswer))
        {
            bool flag = false;
            foreach (string s in badAnswers)
            {
                if (output.Contains(s))
                {
                    Debug.Log("Contains Err of: " + s);
                    flag = true;
                }
            }
            if (!flag)
            {
                print("Answer is right");
                gameObject.SetActive(false);
                ClearAll();
                goalManager.i += 1;
            }
            else
            {
                Debug.Log("Wrong Answer");
                if (errorSubtitles)
                {

                    Instantiate(errorSubtitles, GameObject.Find("SubtitlesSpawnPoint").transform);
                }
                //TODO: add "wrong" subtitles
                eventHandler.AddErrs();
            }
        }
        else
        {
            Debug.Log("Wrong Answer");
            if (errorSubtitles)
            {

                Instantiate(errorSubtitles, GameObject.Find("SubtitlesSpawnPoint").transform);
            }
            //TODO: add "wrong" subtitles
            eventHandler.AddErrs();
        }

    }

    
}
