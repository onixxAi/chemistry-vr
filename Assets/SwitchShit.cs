﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class SwitchShit : MonoBehaviour
{
    public GameObject klava;
    public GameObject table;
    public GameObject manager;
    public GameObject thisis;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Interactable>().isHovering == true)
        {
            if (SteamVR_Actions.default_GrabPinch.GetStateDown(SteamVR_Input_Sources.Any))
            {
                klava.SetActive(true);
                table.SetActive(true);
                manager.SetActive(false);
                thisis.SetActive(false);
                
            }
        }

    }
}
