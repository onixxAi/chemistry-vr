﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiCube : MonoBehaviour
{
    public float weight;
    public Vector3 startScale;
    public bool melt;
    public float time;
    public  bool kek;

    private void Start()
    {
        startScale = transform.localScale;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (!kek)
        {
            if (other.GetComponent<WaterControlling>())
            {

                //transform.localScale = new Vector3(Mathf.Lerp(startScale.x, 0, time), Mathf.Lerp(startScale.x, 0, time), Mathf.Lerp(startScale.x, 0, time));
                melt = true;
            }

        }
        
    }
    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.collider.CompareTag("Flask"))
    //    {

    //        //transform.localScale = new Vector3(Mathf.Lerp(startScale.x, 0, time), Mathf.Lerp(startScale.x, 0, time), Mathf.Lerp(startScale.x, 0, time));
    //        melt = true;
    //    }

    //}
    private void Update()
    { if (!kek)
        {
            if (time < 1)
            {
                if (melt)
                {
                    time += Time.deltaTime / 10;
                }
            }
            transform.localScale = new Vector3(Mathf.Lerp(startScale.x, 0, time), Mathf.Lerp(startScale.x, 0, time), Mathf.Lerp(startScale.x, 0, time));
            if (transform.localScale.x < .1f)
            {
                GameObject.FindObjectOfType<AlkalineCooking>().cubeWeight += weight;
                Destroy(gameObject);
            }
        }
            

    }
}
