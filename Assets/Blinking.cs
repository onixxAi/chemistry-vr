﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blinking : MonoBehaviour
{
    public bool touch;
    public void Update()
    {
        if (!touch)
            GetComponent<SpriteRenderer>().color = new Color(GetComponent<SpriteRenderer>().color.r, GetComponent<SpriteRenderer>().color.g, GetComponent<SpriteRenderer>().color.b, Mathf.PingPong(Time.time * 2, 1) - 0f);
    }
    private void OnTriggerEnter(Collider other)
    {
        touch = true;
        GetComponent<SpriteRenderer>().color = Color.green;
    }


}
