﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyEnter : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        gameObject.SetActive(false);
    }
    private void OnCollisionEnter(Collision collision)
    {

        gameObject.SetActive(false);
    }
}
