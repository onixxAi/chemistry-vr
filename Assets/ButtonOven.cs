﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class ButtonOven : MonoBehaviour
{
    private OvenController controller;
    public string name;
    // Start is called before the first frame update
    void Start()
    {
        controller = FindObjectOfType<OvenController>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Interactable>().isHovering == true)
        {
            if (SteamVR_Actions.default_GrabPinch.GetStateDown(SteamVR_Input_Sources.Any))
            {
                if (name == "Vacuum")
                {
                    if (!controller.capOpened)
                    {
                        controller.vacuum = true;
                    }
                    
                }
                else if (name == "Clear") controller.Clear();
                else if(name =="Bake")
                {
                    if (controller.vacuum && !controller.isUsed) controller.coock = true;
                }


            }
        }
    }
}
