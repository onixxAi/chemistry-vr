﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using TMPro;
public class ButtonElementChooser : MonoBehaviour
{
    // Start is called before the first frame update
    DispenserController controller;
    public Color color;
    ChangeColorButton  changer;
    private bool pushed = false;
    void Start()
    {
        controller = FindObjectOfType<DispenserController>();
        changer = GetComponent<ChangeColorButton>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Interactable>().isHovering == true)
        {
            if (SteamVR_Actions.default_GrabPinch.GetStateDown(SteamVR_Input_Sources.Any))
            {
                if (!controller.ButtonPusshed)
                {
                    controller.SpawnElement(GetComponentInChildren<TextMeshPro>().text, color, gameObject);
                    pushed = true;
                    controller.ButtonPusshed = !controller.ButtonPusshed;
                    changer.SetPushed(pushed);
                }
                else if (pushed)
                {
                    controller.ButtonPusshed = !controller.ButtonPusshed;
                    pushed = false;
                    changer.SetPushed(pushed);
                }
                
                
                

            }
        }
    }
    public void Push()
    {
        controller.ButtonPusshed = !controller.ButtonPusshed;
        pushed = false;
        changer.SetPushed(pushed);
    }
}
