﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class DestilatorElementsControlling : MonoBehaviour
{
    public Vector3 rightPosition;
    public Vector3 rightRotation;
    public string triggerName;
    public bool prishloTvoeVremya;
    public bool wrongAnswer;
    public bool lastElement;
    

    public DestilatorElementsControlling element;
    //public bool prishloTvoeVremya;

    private void OnCollisionEnter(Collision collision)
    {
        if (wrongAnswer)
        {
            Debug.Log("WrongObject");
        }
        if (prishloTvoeVremya)
        {
            Debug.Log("CollisionEnter of" + gameObject.name +": " + collision.gameObject.name);
            if (collision.gameObject.name == triggerName)
            {
                if (element)
                {
                    element.prishloTvoeVremya = true;
                }
                Debug.Log("RightElement");
                GetComponent<Rigidbody>().isKinematic = true;
                GetComponent<Interactable>().enabled = false;
                GetComponent<Throwable>().enabled = false;
                Destroy(GetComponent<Throwable>());
                transform.SetPositionAndRotation (rightPosition, Quaternion.Euler(rightRotation));
                if(lastElement)
                {
                    GameObject.FindObjectOfType<HeaterController>().isReady = true;
                }
            }
        }
    }
    private void Update()
    {
        
    }
}
