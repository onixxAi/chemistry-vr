﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public string lastName,middleName, firstName;
    public bool learning, diagnostic;
    public bool helpControl;
    public bool subtitlesOn;
    public bool hintsOn;
    public TextMeshProUGUI lastNameText, middleNameText, firstNameText;
    public Sprite orangeButton;
    public Sprite dianosticOrangeSprite, diagnosticBlueSprite, learningOrangeSprite, learningBlueSprite;
    public GameObject mainMenu;
    public Canvas operatorCanvas;
    public TextMeshProUGUI helpControlText;
    public Image circleHelpControl;
    public float timeHelpControl;
    public TextMeshProUGUI subtitilesOnText;
    public Image circleSubtitilesOn;
    public float timeSubtitilesOn;
    public TextMeshProUGUI hintsOnText;
    public Image circleHintsOn;
    public float timeHintsOn;
    public Fade fadeCanvas;

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    private void Update()
    {
        circleHintsOn.rectTransform.anchoredPosition = new Vector2(Mathf.Lerp(-70, 70, timeHintsOn), circleHintsOn.rectTransform.anchoredPosition.y);
        circleHelpControl.rectTransform.anchoredPosition = new Vector2(Mathf.Lerp(-70, 70, timeHelpControl), circleHelpControl.rectTransform.anchoredPosition.y);
        circleSubtitilesOn.rectTransform.anchoredPosition = new Vector2(Mathf.Lerp(-70, 70, timeSubtitilesOn), circleSubtitilesOn.rectTransform.anchoredPosition.y);

        if (helpControl)
        {
            if (timeHelpControl <= 1)
            {
                helpControlText.text = "";
                timeHelpControl += Time.deltaTime* 10;
            }
            else
            {
                helpControlText.text = "Вкл";
            }
        }
        else
        {
            if (timeHelpControl >= 0)
            {
                helpControlText.text = "";
                timeHelpControl -= Time.deltaTime* 10;
            }
            else
            {
                helpControlText.text = "             Выкл";
                //helpControlText.text = "     Выкл";
            }
        }
        if (subtitlesOn)
        {
            if (timeSubtitilesOn <= 1)
            {
                subtitilesOnText.text = "";
                timeSubtitilesOn += Time.deltaTime * 10;
            }
            else
            {
                subtitilesOnText.text = "Вкл";
            }
        }
        else
        {
            if (timeSubtitilesOn >= 0)
            {
                subtitilesOnText.text = "";
                timeSubtitilesOn -= Time.deltaTime * 10;
            }
            else
            {
                subtitilesOnText.text = "             Выкл";
            }
        }
        if (hintsOn)
        {
            if (timeHintsOn <= 1)
            {
                hintsOnText.text = "";
                timeHintsOn += Time.deltaTime * 10;
            }
            else
            {
                hintsOnText.text = "Вкл";
            }
        }
        else
        {
            if (timeHintsOn >= 0)
            {
                hintsOnText.text = "";
                timeHintsOn -= Time.deltaTime * 10;
            }
            else
            {
                hintsOnText.text = "             Выкл";
            }
        }
    }
    public void ChangeLastName()
    {
        lastName = lastNameText.text;
        if (lastName != null)
        {
            lastNameText.GetComponentInParent<Image>().sprite = orangeButton;
        }
    }
    public void ChangeMiddleName()
    {
        middleName = middleNameText.text;
        if (middleName != null)
        {
            middleNameText.GetComponentInParent<Image>().sprite = orangeButton;
        }
    }
    public void ChangeFirstName()
    {
        firstName = firstNameText.text;
        if (firstName != null)
        {
            firstNameText.GetComponentInParent<Image>().sprite = orangeButton;
        }
    }
    public void HintsOn()
    {
        hintsOn = !hintsOn;
    }
    public void HelpControl()
    {
        helpControl = !helpControl;
    }
    public void SubtitlesOn()
    {
        subtitlesOn = !subtitlesOn;
    }
    public void StartGame()
    {
        if (lastName != ""&& middleName != ""&& firstName != "")
        {
            print("StartGame");
            mainMenu.GetComponent<Canvas>().enabled = false;
            operatorCanvas.enabled = true;
            operatorCanvas.GetComponent<OperatorCanvas>().enabled = true;
            fadeCanvas.fadeOut=true;
            FindObjectOfType<CSVsaver>().NameOfStudent = lastName + firstName + middleName;
            FindObjectOfType<GoalManager>().i += 1;
        }
    }
    public void Restart()
    {
        FindObjectOfType<GoalManager>().i = -1;
        SceneManager.LoadScene(0);
        Destroy(this);
    }
}
