﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class UISubManager : MonoBehaviour
{
    public GameObject DestroySub;
    public TextMeshProUGUI text;
    public UISub[] uiSub = new UISub[0];
    [HideInInspector]
    public AudioSource audio;
    public int i = 0;

    public bool GoalManagerOff;

    private void Start()
    {
        audio = GetComponent<AudioSource>();
        for (int i = 0; i < uiSub.Length; i++)
        {
            uiSub[i].ui = GetComponent<UISubManager>();
        }
    }
    private void Awake()
    {
        audio = GetComponent<AudioSource>();
        audio.PlayOneShot(uiSub[i].sound);
        text.text = uiSub[i].word;
    }
    private void Update()
    {
        if (i < uiSub.Length)
        {

            text.text = uiSub[i].word;
            uiSub[i].nextM();
        }
        if (i== uiSub.Length)
        {
            text.text = "";
            if (!GoalManagerOff)
            {
                GameObject.FindObjectOfType<GoalManager>().i += 1;
            }
            if (DestroySub)
            {

                Destroy(DestroySub);
            }
        }

    }
}
