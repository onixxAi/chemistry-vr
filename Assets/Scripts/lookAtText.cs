﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class lookAtText : MonoBehaviour
{
    private TextMeshPro tm;
    private void Start()
    {
        tm = GetComponent<TextMeshPro>();
    }
    private void Update()
    {
        if(tm.text != "")
        {
            transform.LookAt(transform.position + Camera.main.transform.rotation *Vector3.forward, Camera.main.transform.rotation*Vector3.up);
        }
    }
}
