﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[System.Serializable]
public class UISub
{
    public enum WordType
    {
        time = 0,
        button=1
    }
    public float timeWord = 2f;
    [HideInInspector]
    public UISubManager ui;
    public string word;
    public AudioClip sound;
    public WordType wordType;
    
    public bool nextM()
    {
        
        if (wordType == WordType.time)
        {
            timeWord -= Time.deltaTime;
            if (timeWord <= 0)
            {


                NextMessage();
                return true;
            }
        }
        if (wordType == WordType.button)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                NextMessage();

                return true;
            }
        }
        return false;
    }

    public void NextMessage()
    {
        ui.i += 1;
        if (ui.audio != null)
        {
            ui.audio.PlayOneShot(ui.uiSub[ui.i].sound);
        }
    }
}
