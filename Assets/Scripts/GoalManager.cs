﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalManager : MonoBehaviour
{
    public int i = 0;
    public int checkPointIndex;
    public Goal[] goal = new Goal[0];
    private void Start()
    {

        for (int i = 0; i < goal.Length; i++)
        {
            goal[i].goalManager = GetComponent<GoalManager>();
        }
    }
    private void Update()
    {
        if (i < goal.Length)
        {
            goal[i].isComplited();
        }
        if (i == goal.Length)
        {
            enabled = false;
        }

    }
}
