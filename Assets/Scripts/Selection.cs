﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Selection : MonoBehaviour
{
    [SerializeField] private Material defaultMaterial;
    [SerializeField] private Material selectableMaterial;
    private Camera _camera;
    [SerializeField]private float rayDistance = 4f;

    private Transform _selection;

    private void Start()
    {
        _camera = GetComponent<Camera>();
    }

    private void FixedUpdate()
    {
        if (_selection != null)
        {
            var selectionRenderer = _selection.GetComponent<Renderer>();
            selectionRenderer.material = defaultMaterial;
            //var outline = _selection.GetComponent<Outline>();
            var _hideSubstance = _selection.GetComponent<ShowSubstance>();
            //if (outline != null) outline.OutlineWidth = 0;
            //_hideSubstance.hideSubstance();
            _selection = null;
        }
        Ray ray = _camera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));

        _selection = null;
        Debug.DrawRay(transform.position, ray.direction * rayDistance, Color.red, 0, false);
        if (Physics.Raycast(ray, out var hit, maxDistance: rayDistance))
        {

            var selection = hit.transform;
            if (selection.CompareTag("TouchButton"))
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    selection.SendMessage("Touch");
                }
            }
            if (selection.CompareTag("Selectable"))
            {
                var selectionRenderer = selection.GetComponent<Renderer>();
                if (selection != null)
                {

                    defaultMaterial = selectionRenderer.material;
                    selectionRenderer.material = selectableMaterial;
                    //var outline = selection.GetComponent<Outline>();
                    var _showSubstance = selection.GetComponent<ShowSubstance>();
                    selectionRenderer.material = selectableMaterial;
                    //if(outline!=null) outline.OutlineWidth = 10;
                    //_showSubstance.showSubstance();

                    //if (Input.GetKey(KeyCode.Mouse0))
                    //{
                    //    outline.OutlineColor = Color.red;
                    //}
                    //else outline.OutlineColor = Color.blue;
                }
                _selection = selection;
            }
        }
    }
}
