﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Valve.VR.InteractionSystem;

public class ShowSubstance : MonoBehaviour
{
    public TextMeshPro tm;

    public string nameSubstance;
    
    private void Update()
    {
        if (GetComponent<Interactable>())
        {
            if (GetComponent<Interactable>().isHovering == true)
            {
                tm.text = nameSubstance;
            }
            else
            {

                tm.text = "";
            }
        }
    }
}
