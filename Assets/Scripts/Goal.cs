﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Goal
{

    public enum GoalTypes
    {
        button,
        mixSubstance,
        time,
        fadeOut,
        fadeIn,
        setActive,
        setDeactivate,
        handTimerON,
        subtitlesON,
        handTimerOFF,
        stageClear
    }
    public string annotation;
    public bool time;
    [HideInInspector]
    public GoalManager goalManager;
    public GoalTypes goalTypes;
    [DrawIf("goalTypes", GoalTypes.button)]
    public TouchButton button;
    [DrawIf("goalTypes", GoalTypes.mixSubstance)]
    public GameObject substanceA;
    [DrawIf("goalTypes", GoalTypes.mixSubstance)]
    public GameObject substanceB;
    [DrawIf("goalTypes", GoalTypes.setActive)]
    public GameObject[] spawnObjects;
    [DrawIf("goalTypes", GoalTypes.setDeactivate)]
    public GameObject[] deactivateObjects;
    [DrawIf("goalTypes", GoalTypes.subtitlesON)]
    public GameObject subtitles;
    [DrawIf("time", true)]
    public float timeWord = 0;
    [DrawIf("goalTypes", GoalTypes.stageClear)]
    public string destroyName;


    [DrawIf("goalTypes", GoalTypes.handTimerON)]
    public int minutes=5;
    [DrawIf("goalTypes", GoalTypes.handTimerON)]
    public bool firstMission;

    [DrawIf("goalTypes", GoalTypes.fadeIn)]
    public string nameMission;

    [DrawIf("goalTypes", GoalTypes.fadeIn)]
    public Transform missionSpawnPoint;

    [DrawIf("goalTypes", GoalTypes.fadeIn)]
    public GameObject instantiateMission;

    [DrawIf("goalTypes", GoalTypes.fadeIn)]
    public Vector3 StartPosition;

    [HideInInspector]
    public bool isComplited()
    {
        if (timeWord > 0)
            timeWord -= Time.deltaTime;
        if (timeWord <= 0)
        {
            if (goalTypes == GoalTypes.time)
            {
                CheckGoalComplited();
                return true;
            }
            if (goalTypes == GoalTypes.button)
            {
                if (GameObject.Find("Pusk"))
                    GameObject.Find("Pusk").SetActive(true);
                if (button.isTouching)
                {
                    CheckGoalComplited();
                    return true;
                }
            }
            if (goalTypes == GoalTypes.fadeIn)
            {
                goalManager.checkPointIndex = goalManager.i;
                if (instantiateMission)
                {

                    GameObject.FindObjectOfType<UpdateMission>().UpdateMyMission(instantiateMission, nameMission, missionSpawnPoint);
                }
                GameObject.Find("Player").transform.position = StartPosition;
                GameObject.FindObjectOfType<Fade>().fadeIn = true;

                CheckGoalComplited();
                return true;
            }
            if (goalTypes == GoalTypes.fadeOut)
            {
                GameObject.FindObjectOfType<Fade>().fadeOut = true;
                CheckGoalComplited();
                return true;
            }
            if (goalTypes == GoalTypes.setActive)
            {
                for (int i = 0; i < spawnObjects.Length; i++)
                {
                    spawnObjects[i].SetActive(true);

                }
                return true;
            }
            if (goalTypes == GoalTypes.setDeactivate)
            {
                for (int i = 0; i < deactivateObjects.Length; i++)
                {
                    deactivateObjects[i].SetActive(false);

                }
                CheckGoalComplited();
                return true;
            }
            if (goalTypes == GoalTypes.handTimerON)
            {
                if (GameObject.FindObjectOfType<HandTimer>())
                {


                    GameObject.FindObjectOfType<HandTimer>().minutes = minutes;
                    GameObject.FindObjectOfType<HandTimer>().seconds = 0;
                    GameObject.FindObjectOfType<HandTimer>().timerON = true;
                    if (firstMission)
                    {
                        GameObject.FindObjectOfType<HandTimer>().firstmission = true;
                    }
                    else
                    {
                        GameObject.FindObjectOfType<HandTimer>().firstmission = false;
                    }
                }
                GameObject.Find("BackEnd").GetComponent<UniversalTimer>().currenTime = minutes*60;
                CheckGoalComplited();
                return true;
            }
            if (goalTypes == GoalTypes.handTimerOFF)
            {
                GameObject.FindObjectOfType<HandTimer>().timerON = false;
                CheckGoalComplited();
                return true;
            }
            if (goalTypes == GoalTypes.subtitlesON)
            {

                if (!GameObject.FindObjectOfType<UISubManager>()) GameObject.Instantiate(subtitles, GameObject.Find("SubtitlesSpawnPoint").transform);
                return true;
            }
            if( goalTypes == GoalTypes.stageClear)
            {
                if (destroyName!="(Clone)")
                {
                    GameObject.Destroy(GameObject.Find(destroyName));
                }
                GameObject.Find("BackEnd").GetComponent<PlayerEventsStorage>().PushTaskResults(GameObject.Find("BackEnd").GetComponent<UniversalTimer>().GetCurrentTime());
                CheckGoalComplited();
            }
        }
        return false;

    }

    public void CheckGoalComplited()
    {
        goalManager.i += 1;
    }
}

