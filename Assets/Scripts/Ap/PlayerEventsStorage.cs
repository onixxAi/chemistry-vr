﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    public class PlayerEventsStorage : MonoBehaviour
    {
        private int hintsCounts = 0;
        private int errsCount = 0;
        private int step = 1;

        hg.ApiWebKit.apis.example.media.ResultsAgrigator agrigator;

        // Start is called before the first frame update
        void Start()
        {
            agrigator = gameObject.GetComponent<hg.ApiWebKit.apis.example.media.ResultsAgrigator>();
        }
        
        public void AddHints()
    {
        hintsCounts++;

    }
    public void AddErrs()
    {
        errsCount++;
        Debug.Log("added err to event handler, curr is: " + errsCount);
    }
    public void PushTaskResults(float time)
    {
        agrigator.ObtainResults(errsCount, time, step, hintsCounts);
        hintsCounts = 0;
        errsCount = 0;
        step++;

        if (step > 10) step = 1;
    }
    public void ClearResults()
    {
        hintsCounts = 0;
        errsCount = 0;
       
    }

    // Update is called once per frame

}

