﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace hg.ApiWebKit.apis.example.media
{
    
    public class ResultsAgrigator : MonoBehaviour
    {
        Controlpoint controlpoint;
        CSVsaver saver;

        private int[] maxErrs =
        {
        3,
        4,
        3,
        5,
        2,
        3,
        3,
        2,
        2,
        2
        };

        private float[] maxTime =
        {
        5 * 60f,
        5 * 60f,
        5 * 60f,
        10 * 60f,
        5 * 60f,
        5 * 60f,
        5 * 60f,
        3 * 60f,
        5 * 60f,
        5 * 60f
        };

        //!!!!!!
        public void ObtainResults(float errs, float time, int stepNum, int hints)
        {
            int timeScore = 0;
            int errScore = 0;
            float errCoef = errs / maxErrs[stepNum - 1];
            float timeCoef = time / maxTime[stepNum - 1];
            Debug.Log("errCoef " + errCoef);
            Debug.Log("timeCoef " + timeCoef);

            if (errCoef <= 1f) errScore = 7;
            else if (errCoef > 1f && errCoef < 1.5f) errScore = 5;
            else if (errCoef >= 1.5f && errCoef < 2f) errScore = 3;
            else if (errCoef >= 2f && errCoef < 2.5f) errScore = 1;
            else errScore = 0;

            if (timeCoef <= 0.5f) timeScore = 3;
            else if (timeCoef > 0.5f && timeCoef < 0.75f) timeScore = 2;
            else if (timeCoef >= 0.75f && timeCoef < 1) timeScore = 1;
            else timeScore = 0;
            Debug.Log("ErrScore: " + errScore);
            Debug.Log("TimeScore: " + timeScore);
            controlpoint.hintsSumm += hints;
            saver.WriteData(time, timeScore + errScore, hints, stepNum);
            controlpoint.curBalls.Add(timeScore + errScore);


        }

        // Start is called before the first frame update
        void Start()
        {
            controlpoint=  gameObject.GetComponent<Controlpoint>();
            saver = gameObject.GetComponent<CSVsaver>();
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}