﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using hg.ApiWebKit.core.http;
using hg.ApiWebKit.core.attributes;
using hg.ApiWebKit.mappers;

[HttpGET]
[HttpPath(null, "http://vrdmn.mcko.ru:3001/api/getsnapshot")]
[HttpAccept("application/json")]
[HttpProvider(typeof(hg.ApiWebKit.providers.HttpUnityWebRequestClient))]
public class GetSnapshot : HttpOperation
{
    [HttpQueryString("id")]
    public string Id;

    [HttpQueryString("code")]
    public string Code;

    [HttpQueryString("currentstep")]
    public int Currentstep;

    [HttpQueryString("donelist")]
    public string Donelist;

    [HttpQueryString("state")]
    public int Step;

    [HttpResponseJsonBody]
    public respModel.ResponseModel Response;

    protected override void FromResponse(HttpResponse response)
    {
        base.FromResponse(response);
    }

}
