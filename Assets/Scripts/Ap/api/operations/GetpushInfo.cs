﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using hg.ApiWebKit.core.http;
using hg.ApiWebKit.core.attributes;
using hg.ApiWebKit.mappers;

namespace hg.ApiWebKit.apis.example.media
{
    [HttpGET]
    [HttpPath(null, "http://vrdmn.mcko.ru:3001/api/pushinfo")]
    [HttpAccept("application/json")]
    [HttpProvider(typeof(hg.ApiWebKit.providers.HttpUnityWebRequestClient))]
    public class GetpushInfo : HttpOperation
    {
        [HttpQueryString("id")]
        public string Id;

        [HttpQueryString("name")]
        public string Name;

        [HttpQueryString("ver")]
        public string Version;

        [HttpQueryString("desc")]
        public string Description;


        [HttpResponseJsonBody]
        public respModel.ResponseModel Response;

        protected override void FromResponse(HttpResponse response)
        {
            base.FromResponse(response);
        }
    }
}
