﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using hg.ApiWebKit.core.http;
using hg.ApiWebKit.core.attributes;
using hg.ApiWebKit.mappers;

namespace hg.ApiWebKit.apis.example.media
{
    [HttpGET]
    [HttpPath(null, "http://vrdmn.mcko.ru:3001/api/getcommand")]
    [HttpAccept("application/json")]
    [HttpProvider(typeof(hg.ApiWebKit.providers.HttpUnityWebRequestClient))]
    public class GetCommand : HttpOperation
    {

        [HttpQueryString("id")]
        public string Id;

        [HttpQueryString("batt")]
        public int Batt;

        [HttpQueryString("temp")]
        public int Temp;


        [HttpResponseJsonBody]
        public respControl.CommandResponse Response;

        protected override void FromResponse(HttpResponse response)
        {
            base.FromResponse(response);
        }
    }
}
