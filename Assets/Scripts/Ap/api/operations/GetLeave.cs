﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using hg.ApiWebKit.core.http;
using hg.ApiWebKit.core.attributes;
using hg.ApiWebKit.mappers;

[HttpGET]
[HttpPath("http://vrdmn.mcko.ru:3001/api", "/leave")]
[HttpAccept("application/json")]
[HttpProvider(typeof(hg.ApiWebKit.providers.HttpUnityWebRequestClient))]
public class GetLeave : HttpOperation
{
    [HttpQueryString("id")]
    public string Id;

    [HttpResponseJsonBody]
    public respModel Response;

    protected override void FromResponse(HttpResponse response)
    {
        base.FromResponse(response);
    }
}
