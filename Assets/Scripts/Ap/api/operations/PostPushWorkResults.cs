﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using hg.ApiWebKit.core.http;
using hg.ApiWebKit.core.attributes;
using hg.ApiWebKit.mappers;

namespace hg.ApiWebKit.apis.example.media
{
    [HttpPOST]
    [HttpPath(null, "http://vrdmn.mcko.ru:3001/api/pushworkresult")]
    [HttpAccept("application/json")]
    [HttpContentType("application/json")]
    [HttpProvider(typeof(hg.ApiWebKit.providers.HttpUnityWebRequestClient))]
    public class PostPushWorkResults : HttpOperation
    {
        [HttpRequestJsonBody]
        public Sender Request;

        public class Sender
        {
            public string id;
            public Result result;
            public string metadata;
        }

        public class Result
        {
            public string request_code;
            public int primary_mark;
            public int percent_mark;
            public string test_result_b;
            public string result_c_date;
            public string test_result_c;
            public Task[] tasks;
        }
        public class Task
        {
            public int task_number;
            public int max_ball;
            public int ball;
            public string[] criteria;
            public Theme[] Themes;

        }
        public class Theme
        {
            public string Name;
        }

        [HttpResponseJsonBody]
        public respModel.ResponseModel Response;

        protected override void FromResponse(HttpResponse response)
        {
            base.FromResponse(response);
        }
    }
}
