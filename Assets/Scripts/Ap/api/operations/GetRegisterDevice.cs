﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using hg.ApiWebKit.core.http;
using hg.ApiWebKit.core.attributes;
using hg.ApiWebKit.mappers;

namespace hg.ApiWebKit.apis.example.media
{
    [HttpGET]
    [HttpPath(null, "http://vrdmn.mcko.ru:3001/api/registerdevice")]
    [HttpAccept("application/json")]
    [HttpProvider(typeof(hg.ApiWebKit.providers.HttpUnityWebRequestClient))]
    public class GetRegisterDevice : HttpOperation
    {
        [HttpQueryString("id")]
        public string Id;

        [HttpQueryString("status")]
        public int Status;



        [HttpResponseJsonBody]
        public respModel.ResponseModel Response;

        protected override void FromResponse(HttpResponse response)
        {
            base.FromResponse(response);
        }
    }
}

