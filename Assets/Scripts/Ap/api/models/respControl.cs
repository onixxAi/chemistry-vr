﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class respControl : MonoBehaviour
{
    public class CommandResponse
    {
        public string resp;
        public string command;
        public Args args;
    }
    public class Args
    {
        public string param1;
        public string param2;
        public string param3;
        public string param4;
        public string param5;
    }
}
