﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace hg.ApiWebKit.apis.example.media
{
    public class Themes : MonoBehaviour
    {
        public PostPushWorkResults.Theme[] theme1 =
        {
            new PostPushWorkResults.Theme()
            {
                Name = "Периодическая система, химический элемент, знаки химических элементов"

            },
            new PostPushWorkResults.Theme()
            {
                Name = "Простые газы"
            },
            new PostPushWorkResults.Theme()
            {
                Name = "Состав воздуха"
            }


        };

        public PostPushWorkResults.Theme[] theme2 =
            {
            new PostPushWorkResults.Theme()
            {
                Name = "Умение читать и составлять химические уравнения"

            },
            new PostPushWorkResults.Theme()
            {
                Name = "Умение составлять химические уравнения с продуктами в виде простых веществ"
            },
            new PostPushWorkResults.Theme()
            {
                Name = "Реакции разложения"
            },
            new PostPushWorkResults.Theme()
            {
                Name = "Знание промышленных способов получения кислорода"
            }
        };

        public PostPushWorkResults.Theme[] theme3 =
            {
            new PostPushWorkResults.Theme()
            {
                Name = "Умение читать и составлять химические уравнения"

            },
            new PostPushWorkResults.Theme()
            {
                Name = "Умение составлять химические уравнения с продуктами в виде простых веществ"
            },
            new PostPushWorkResults.Theme()
            {
                Name = "Реакции разложения"
            },
            new PostPushWorkResults.Theme()
            {
                Name = "Знание промышленных способов получения кислорода"
            }
        };

    }
}
