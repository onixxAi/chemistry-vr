﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using System.Linq;

namespace hg.ApiWebKit.apis.example.media
{
    public class Controlpoint : MonoBehaviour
    {
        private float timer = 7.0f;
        private bool is_sending = false;
        private bool registered = false;
        private bool anticipatorily = false;
        private string cryptoid = "";
        private ResultsAgrigator agrigator;
        private CSVsaver saver;
        public int hintsSumm = 0;

        //!!!!
        public bool pauseWork = false;
        public bool mainMenue = true;
        //!!!!

        private bool finished = false;
        private int curStatus = 101; //101-start, 102-during test, 103-test is ended
        public List<int> curBalls = new List<int>();
        private string diagCode = "";
        private string[] themes =
        { "Воздух",
         "Кислород",
         "Техника безопасности при авариях",
          "Анализ вещества",
          "Дистилляция воды",
          "Приготовление щёлочи",
          "Запись в лабораторный журнал",
          "Поломка крио-капсулы",
          "Диагностика крио-капсулы",
          "Ремонт крио-капсулы"};





        // Start is called before the first frame update
        void Start()
        {

            agrigator = gameObject.GetComponent<ResultsAgrigator>();
            saver = gameObject.GetComponent<CSVsaver>();
            Debug.Log(ListToString(curBalls));
            cryptoid = SystemInfo.deviceUniqueIdentifier;
            StartCoroutine("SendRegister");


        }
        public int SummScore()
        {
            return curBalls.Sum();
        }

        // Update is called once per frame
        void Update()
        {
            if (!is_sending && registered)
            {
                timer -= Time.deltaTime;
                if (timer <= 0f)
                {
                    StartCoroutine("SendGetCommand");
                    timer = 7.0f;
                }
                if (curBalls.Count == 10 && !finished)
                {
                    StartCoroutine("SendPushWorkInfo");
                    saver.Save();
                    finished = true;
                }
            }
            else if (!registered)
            {
                StartCoroutine("SendRegister");
            }


        }
        #region Enumerators to send

        IEnumerator SendGetCommand()
        {
            is_sending = !is_sending;
            new GetCommand
            {

                Id = cryptoid,
                Batt = -1,
                Temp = -1
            }.Send(OnSuccessCommand, OnFailureCommand, OnCompleteCommand);

            yield break;
        }
        IEnumerator SendPushWorkInfo()
        {
            new PostPushWorkResults
            {
                Request = new PostPushWorkResults.Sender()
                {
                    id = cryptoid,
                    result = new PostPushWorkResults.Result()
                    {
                        request_code = diagCode,
                        primary_mark = curBalls.Sum(),
                        percent_mark = curBalls.Sum(),
                        test_result_b = "",
                        result_c_date = "",
                        test_result_c = ListtoWork(curBalls),
                        tasks = TasksParser()


                    },
                    metadata = anticipatorily ? "Досрочно" : ""
                }
            }.Send(OnSuccessPushWork, OnFailurePushWork, OnCompletePushWork);

            yield break;
        }
        IEnumerator SendPushInfo()
        {
            new GetpushInfo
            {
                Id = cryptoid,
                Name = "VR-диагностика по химии",
                Version = "V1.5",
                Description = ""
            }.Send(OnSuccessPushinfo, OnFailurePushinfo, OnCompletePushifo);
            yield break;
        }
        IEnumerator SendSnapshot()
        {
            if (!mainMenue)
            {

                new GetSnapshot
                {
                    Id = cryptoid,
                    Code = diagCode,
                    Currentstep = curBalls.Count + 1,
                    Donelist = ListToString(curBalls),
                    Step = pauseWork ? 1 : 0
                }.Send(OnSuccessSanp, OnFailureSnap, OnCompleteSnap);
            }

            yield break;
        }

        IEnumerator SendRegister()
        {
            new GetRegisterDevice
            {
                Id = cryptoid,
                Status = curStatus
            }.Send(OnSuccessRegister, OnFailureRegister, OnCompleteREgister);
            yield break;
        }

        #endregion

        #region Listeners
        #region Register
        private void OnSuccessRegister(GetRegisterDevice operation, core.http.HttpResponse response)
        {
            Debug.Log("Success");

            Debug.Log(operation.Response.resp);
            registered = true;
        }

        private void OnFailureRegister(GetRegisterDevice operation, core.http.HttpResponse response)
        {
            Debug.Log("Failed");

            Debug.Log("Faulted because: " + string.Join(" ; ", operation.FaultReasons.ToArray()));
        }

        private void OnCompleteREgister(GetRegisterDevice operation, core.http.HttpResponse response)
        {
            Debug.Log("Completed");

        }
        #endregion

        #region GetCommand

        private void OnSuccessCommand(GetCommand operation, core.http.HttpResponse response)
        {
            Debug.Log("Success");

            Debug.Log(operation.Response.command);
            CommandParser(operation.Response.command, operation.Response.args.param1, operation.Response.args.param2);
            is_sending = !is_sending;

        }

        private void OnFailureCommand(GetCommand operation, core.http.HttpResponse response)
        {
            Debug.Log("Failed");
            StartCoroutine("SendGetCommand");

            Debug.Log("Faulted because: " + string.Join(" ; ", operation.FaultReasons.ToArray()));

        }

        private void OnCompleteCommand(GetCommand operation, core.http.HttpResponse response)
        {
            Debug.Log("Completed");

        }
        #endregion

        #region PUSHWork
        private void OnSuccessPushWork(PostPushWorkResults operation, core.http.HttpResponse response)
        {
            Debug.Log("Success");
            curStatus = 103;
            mainMenue = true;


            Debug.Log(operation.Response.resp);

        }

        private void OnFailurePushWork(PostPushWorkResults operation, core.http.HttpResponse response)
        {
            Debug.Log("Failed");

            Debug.Log("Faulted because: " + string.Join(" ; ", operation.FaultReasons.ToArray()));
        }

        private void OnCompletePushWork(PostPushWorkResults operation, core.http.HttpResponse response)
        {
            Debug.Log("Completed");

        }
        #endregion

        #region PUSHinfo
        private void OnSuccessPushinfo(GetpushInfo operation, core.http.HttpResponse response)
        {
            Debug.Log("Success");

            Debug.Log(operation.Response.resp);

        }

        private void OnFailurePushinfo(GetpushInfo operation, core.http.HttpResponse response)
        {
            Debug.Log("Failed");

            Debug.Log("Faulted because: " + string.Join(" ; ", operation.FaultReasons.ToArray()));
        }

        private void OnCompletePushifo(GetpushInfo operation, core.http.HttpResponse response)
        {
            Debug.Log("Completed");

        }
        #endregion

        #region SnapShot
        private void OnSuccessSanp(GetSnapshot operation, core.http.HttpResponse response)
        {
            Debug.Log("Success");

            Debug.Log(operation.Response.resp);

        }

        private void OnFailureSnap(GetSnapshot operation, core.http.HttpResponse response)
        {
            Debug.Log("Failed");

            Debug.Log("Faulted because: " + string.Join(" ; ", operation.FaultReasons.ToArray()));
        }

        private void OnCompleteSnap(GetSnapshot operation, core.http.HttpResponse response)
        {
            Debug.Log("Completed");

        }
        #endregion

        #endregion



        private void CommandParser(string command, string arg, string arg2)
        {
            switch (command)
            {
                case "exit":
                    //TODO: exit app
                    break;
                case "backToIntro":
                    curStatus = 101;
                    mainMenue = true;
                    //TODO: back to main menu;
                    break;
                case "registerCode":
                    diagCode = arg;
                    if (!string.IsNullOrEmpty(arg2)) saver.NameOfStudent = arg2;

                    break;
                case "runWork":
                    //TODO: run VR test
                    StartTest();
                    break;
                case "pauseWork":
                    pauseWork = !pauseWork;
                    break;
                case "cancelWork":
                    //TODO: Cancel work without saving

                    break;
                case "finishWork":
                    //TODO: finish test and send results via POST
                    anticipatorily = true;
                    StartCoroutine("SendPushWorkInfo");
                    break;
                case "getWorkStatus":
                    StartCoroutine("SendSnapshot");
                    break;
                case "getInfo":
                    StartCoroutine("SendPushInfo");
                    break;
                default:
                    Debug.Log("Server is ONLINE");
                    break;

            }
        }

        private string ListToString(List<int> list)
        {
            string output = "";
            for (int i = 0; i < list.Count; i++)
            {
                if (i != 0) output += ",";
                string l = list[i].ToString();
                output += l;
            }
            return output;

        }
        private string ListtoWork(List<int> list)
        {
            string output = "";
            foreach (int i in list)
            {
                output += i.ToString();
                output += "(10)";

            }
            return output;
        }

        private PostPushWorkResults.Task[] TasksParser()
        {

            List<PostPushWorkResults.Task> taskslist = new List<PostPushWorkResults.Task>();

            for (int i = 0; i < curBalls.Count; i++)
            {
                PostPushWorkResults.Task temp = new PostPushWorkResults.Task();

                PostPushWorkResults.Theme[] themes1 =
                {
                    new PostPushWorkResults.Theme()
                    {
                        Name = ""

                    }

                    };
                themes1[0].Name = themes[i];
                temp.task_number = i + 1;
                temp.ball = curBalls[i];
                temp.max_ball = 10;
                temp.criteria = null;
                temp.Themes = themes1;
                taskslist.Add(temp);

            }
            return taskslist.ToArray();


        }

        public void StartTest()
        {
            finished = false;
            pauseWork = false;
            mainMenue = false;
            curBalls.Clear();
            curStatus = 102;
        }

    }
}
