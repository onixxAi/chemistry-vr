﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace hg.ApiWebKit.apis.example.media {
    public class SenderTest : MonoBehaviour
    {
        // Start is called before the first frame update
        IEnumerator Start()
        {
            new GetRegisterDevice
            {
                Id = "Test1234",
                Status = 101
            }
            .Send(OnSuccess,OnFailure, OnComplete);
            yield break;
        }

        private void OnSuccess(GetRegisterDevice operation, core.http.HttpResponse response)
        {
            Debug.Log("Success");

            Debug.Log(operation.Response.resp);
        }

        private void OnFailure(GetRegisterDevice operation, core.http.HttpResponse response)
        {
            Debug.Log("Failed");

            Debug.Log("Faulted because: " + string.Join(" ; ", operation.FaultReasons.ToArray()));
        }

        private void OnComplete(GetRegisterDevice operation, core.http.HttpResponse response)
        {
            Debug.Log("Completed");
        }

        // Update is called once per frame
        void Update()
        {

        }
        IEnumerator SendWorkResult()
        {
            new PostPushWorkResults
            {
                Request = new PostPushWorkResults.Sender() { }
            }.Send(null, null, null);
            yield break;
        }
    }
}
