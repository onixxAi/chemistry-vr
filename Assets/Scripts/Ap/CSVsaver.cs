﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using System;
public class CSVsaver : MonoBehaviour
{
    private List<string[]> rowData = new List<string[]>();

    public string NameOfStudent;


    public bool switchTimer = false;
    // Start is called before the first frame update
    void Start()
    {
        init();

    }
    float timer = 0;

    // Update is called once per frame
    private void init()
    {
        string[] rowDataTemp = new string[4];
        rowDataTemp[0] = "Номер задания";
        rowDataTemp[1] = "Затраченное время";
        rowDataTemp[2] = "Баллы";
        rowDataTemp[3] = "Кол-во подсказок";

        rowData.Add(rowDataTemp);
    }



    public void WriteData(float time, int score, int hints, int stepNUM)
    {
        string[] rowDataTo = new string[4];
        rowDataTo[0] = stepNUM.ToString();
        rowDataTo[1] = time.ToString();
        rowDataTo[2] = score.ToString();
        rowDataTo[3] = hints.ToString();
        rowData.Add(rowDataTo);
    }








    public void Save()
    {



        string[][] output = new string[rowData.Count][];

        for (int i = 0; i < output.Length; i++)
        {
            output[i] = rowData[i];
        }

        int length = output.GetLength(0);
        string delimiter = ",";

        StringBuilder sb = new StringBuilder();

        for (int index = 0; index < length; index++)
            sb.AppendLine(string.Join(delimiter, output[index]));


        string filePath = getPath();

        StreamWriter outStream = System.IO.File.CreateText(filePath);
        outStream.WriteLine(sb);
        outStream.Close();
        rowData.Clear();
        init();
    }


    private string getPath()
    {
#if UNITY_EDITOR
        return Application.dataPath + "/CSV/" + "ОТЧЕТ" + "-" + "ФИО:" + "-" + NameOfStudent + "-" + DateTime.Now.ToString("MM-dd-yyyy") + "-" + DateTime.Now.ToString("hh-mm-ss") + ".csv";
#elif UNITY_ANDROID
        return Application.persistentDataPath+"Saved_data" +"-" + "Device_Name"+ "-"+ deviceName+ "-"+ DateTime.Now.ToString("MM-dd-yyyy") + "-" + DateTime.Now.ToString("hh-mm-ss") + ".csv";
#elif UNITY_IPHONE
        return Application.persistentDataPath+"/"+"Saved_data" +"-" + "Device_Name"+ "-"+ deviceName+ "-"+ DateTime.Now.ToString("MM-dd-yyyy") + "-" + DateTime.Now.ToString("hh-mm-ss") + ".csv";
#else
        return Application.dataPath +"/"+"ОТЧЕТ" + "-" + "ФИО:" + "-" + NameOfStudent + "-"+ DateTime.Now.ToString("MM-dd-yyyy") + "-" + DateTime.Now.ToString("hh-mm-ss") + ".csv";
#endif
    }
}

