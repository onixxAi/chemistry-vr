﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Valve.VR.InteractionSystem;
using Valve.VR;
using Valve;

[RequireComponent(typeof(AudioSource))]
public class KeyboardButton : MonoBehaviour
{
    public bool isTouching;
    private AudioSource audioData;
    public AudioClip buttonSFX;
    public string nameButton;
    public VRKeyboard input;
    public bool writeSymbol;
    private PlayerEventsStorage eventHandler;
    public bool findValidGazes;
    public bool findGazPercent;
    public bool NormalKeyBoard;
    public bool guess;
    public Vector3 startPos;
    public Quaternion startRotation;
    public Quaternion targetRotation;
    public Vector3 targetPos;
    private ChangeColorButton changerColor;
    private bool buttonPressed = false;
    public GameObject uiCanvas;
    public GameObject AlternativeRightAnswerCanvas;



    private void Awake()
    {
        if (findValidGazes)
        {
            startRotation = transform.rotation;
            startPos = transform.position;
            targetPos = startPos;
            targetRotation = startRotation;

        }
        if (nameButton == "") nameButton = GetComponentInChildren<TextMeshPro>().text;
        isTouching = false;
        audioData = GetComponent<AudioSource>();
        eventHandler = GameObject.Find("BackEnd").GetComponent<PlayerEventsStorage>();
        changerColor = GetComponent<ChangeColorButton>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (findValidGazes)
        {
            if (GetComponent<Rigidbody>().isKinematic)
            {
                if (other.CompareTag("AnswerField"))
                {
                    if (other.GetComponent<TextMeshPro>())
                    {

                        if (other.GetComponent<TextMeshPro>().color != Color.green)
                        {
                            if (nameButton == "validGaz")
                            {
                                GetComponent<Collider>().enabled = false;
                                print("kek");
                                targetPos = other.transform.position;
                                targetRotation = other.transform.rotation;
                                other.GetComponent<TextMeshPro>().color = Color.green;
                                input.CheckAnswer(nameButton);
                            }
                            if (nameButton == "InvalidGaz")
                            {
                                eventHandler.AddErrs();
                                //to do
                                print("Error of gaz selection");

                                //other.GetComponent<TextMeshPro>().enabled = true;
                                other.GetComponent<TextMeshPro>().color = Color.red;
                            }
                        }
                    }

                }
            }

            else
            {
                if (SteamVR_Actions.default_GrabPinch.GetStateDown(SteamVR_Input_Sources.Any))
                {
                    if (other.CompareTag("AnswerField"))
                    {
                        targetPos = other.transform.position;
                        targetRotation = other.transform.rotation;
                    }
                    else
                    {
                        targetPos = startPos;
                        targetRotation = startRotation;
                    }
                }
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (findValidGazes)
        {
            if (other.GetComponent<TextMeshPro>())
            {
                if (other.GetComponent<TextMeshPro>().color != Color.green)
                {
                    if (other.CompareTag("AnswerField"))
                    {
                        other.GetComponent<TextMeshPro>().color = Color.red;
                    }

                }
            }

        }
    }
    public void Touch()
    {
        if (findValidGazes)
        {



            if (nameButton == "validGaz")
            {
                input.CheckAnswer(nameButton);
                gameObject.SetActive(false);
            }
            if (nameButton == "InvalidGaz")
            {
                eventHandler.AddErrs();
                //to do
                print("Error of gaz selection");
            }
        }
        else
        {
            input.CheckAnswer(nameButton);
            audioData.PlayOneShot(buttonSFX);

        }

    }
    private void WriteButtonToString()
    {
        input.GetButtonName(gameObject.GetComponentInChildren<TextMeshPro>().text);
       

        buttonPressed = !buttonPressed;
        changerColor.SetPushed(buttonPressed);


    }
    private void SendNumData()
    {
        
        input.GetDataFromNumBoard(nameButton);
    }

    private void SendKey()
    {
        input.KeyboardManager(nameButton);
    }

    private void Update()
    {
        //transform.eulerAngles = new Vector3(0, 0, 0);
        

        if (GetComponent<Interactable>().isHovering == true)
        {
            if (SteamVR_Actions.default_GrabPinch.GetStateDown(SteamVR_Input_Sources.Any))
            {
                Debug.Log("Pressed");
                if (findValidGazes)
                {
                    if (input.counterOfPushedButtons < input.input.Length || buttonPressed)
                    {
                        Debug.Log("Enterd NoramlBut");
                        if (nameButton != "Enter")
                        {
                            WriteButtonToString();
                        }
                    }
                    if (nameButton == "Enter" && (input.counterOfPushedButtons == input.input.Length || input.containsRightAnswer)) input.CheckAnswerLogic();
                    else if (nameButton == "EnterOxygenGen") input.ChackanswersWithRightAnswers();
                    
                }
                else if(findGazPercent)
                {
                    if(nameButton == "Enter"&& input.counterOfAnswers==4)
                    {
                        input.CheckAnswerLogic();
                    }
                    else if(nameButton != "Enter")
                    {
                        SendNumData();
                    }
                }
                else if(NormalKeyBoard)
                {
                    if (nameButton == "Enter")
                    {
                        //input.ChackanswersWithRightAnswers();
                        input.ChackanswersWithRightAnswersKeyboard();
                    }
                    else SendKey();
                    
                }
                //SANYA LOGIC
                else if (guess)
                {
                    if (nameButton == "RightAnswer")
                    {
                        Debug.Log("RightAnswer");
                        input.goalManager.i += 1;
                        uiCanvas.SetActive(false);
                    }
                    else if(nameButton == "AlternativeRightAnswer")
                    {
                        Debug.Log("RightAnswer NaHCO3");
                        input.goalManager.i += 1;
                        AlternativeRightAnswerCanvas.SetActive(true);
                        uiCanvas.SetActive(false);
                    }
                    else
                    {
                        Debug.Log("Wrong Answer");
                        input.eventHandler.AddErrs();
                    }
                }
                else Touch();


            }


        }

    }

}
