﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using TMPro;

public class Debuger : MonoBehaviour
{
    public GoalManager manager;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Interactable>().isHovering == true)
        {
            if (SteamVR_Actions.default_GrabPinch.GetStateDown(SteamVR_Input_Sources.Any))
            {
                manager.i += 1;
            }
        }
        GetComponentInChildren<TextMeshPro>().text = "Stage " + manager.i;
    }
}
