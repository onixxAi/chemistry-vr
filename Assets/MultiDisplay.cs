﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiDisplay : MonoBehaviour
{
    void Start()
    {
        if (Display.displays.Length > 1)
        {
            //display 0 is set by default
            Display.displays[1].Activate();
            //Display.displays[1].SetParams(800, height of display 1, starting x for display 1, starting y for display 1) ;
        }
    }
}
