﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class ChangeColorButton : MonoBehaviour
{
    public Color Orange;
    public Color Blue;
    private bool isPushed = false;

    private void Update()
    {
        if (!isPushed)
        {
            if (GetComponent<Interactable>().isHovering == true)
            {
                GetComponent<Renderer>().material.color = Orange;
            }
            else
            {

                GetComponent<Renderer>().material.color = Blue;

            }
        }
    }
    public void SetPushed(bool flag)
    {
        if (flag)
        {
            GetComponent<Renderer>().material.color = Orange;
            isPushed = !isPushed;
        
        }
        else
        {
            GetComponent<Renderer>().material.color = Blue;
            isPushed = !isPushed;
        }
    }
}
