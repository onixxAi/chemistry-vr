﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fade : MonoBehaviour
{
    public float time=1;
    public bool fadeIn;
    public bool fadeOut;
    private void Start()
    {
        
    }
    private void Update()
    {
        if (fadeIn)
        {
            fadeOut = false;
            if (time <1)
            {
                time += Time.deltaTime;
            }
            else
            {
                fadeIn = false;
            }
        }
        if (fadeOut)
        {
            fadeIn = false;
            if (time>0)
            {
                time -= Time.deltaTime;
            }
            else
            {
                fadeOut = false;
            }
        }
        GetComponentInChildren<Image>().color = new Color(GetComponentInChildren<Image>().color.r, GetComponentInChildren<Image>().color.g, GetComponentInChildren<Image>().color.b, Mathf.Lerp(1, 0, time));
    }
}
