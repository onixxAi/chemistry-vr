﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class ChangeKek : MonoBehaviour
{
    public GameObject a;
    public GameObject b;
    public GameObject c;
    int counter =0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Interactable>().isHovering == true)
        {
            if (SteamVR_Actions.default_GrabPinch.GetStateDown(SteamVR_Input_Sources.Any))
            {
                if (counter < 2) counter++;
                else counter = 0;
                switch(counter)
                {
                    case 0:
                        a.SetActive(true);
                        b.SetActive(false);
                        c.SetActive(false);
                        break;
                    case 1:
                        a.SetActive(false);
                        b.SetActive(true);
                        c.SetActive(false);
                        break;
                    case 2:
                        a.SetActive(false);
                        b.SetActive(false);
                        c.SetActive(true);
                        break;

                }

            }
        }
    }
}
