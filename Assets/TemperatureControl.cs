﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TemperatureControl : MonoBehaviour
{

    public TextMeshPro text;

    
    public float temp=21;
    private float cur_temp=0;
    public float time = 0;
    public bool temperatureRewind;
    public bool temperature;
    public bool tempertureOverride;
    private float timer = 5f;
    private float timerForWater = 10f;
    private bool lastTemp = false;
    private bool afterFill = false;
    public HeaterController controller;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (tempertureOverride || temperature || temperatureRewind)
        {
            timer -= Time.deltaTime;

            if (time < 1)
            {

                if (tempertureOverride)
                {
                    time += Time.deltaTime / 50;
                }
                else
                {
                    time += Time.deltaTime / 10;
                }


            }

            if (tempertureOverride)
            {
                temp = Mathf.Lerp(100, 200, time);
                temp = (int)Mathf.Round(temp);
            }
            else if (temperature)
            {
                temp = Mathf.Lerp(21, 100, time);
                temp = (int)Mathf.Round(temp);
            }
            else
            {
                temp = Mathf.Lerp(cur_temp, 21, time);
                temp = (int)Mathf.Round(temp);
            }



            text.text = temp.ToString() + "°C";



            if (temperatureRewind)
            {
                if (temp == 22 && time > 0)
                {
                    time = 0;
                    temp = 21;
                    text.text = temp.ToString() + "°C";
                    temperatureRewind = !temperatureRewind;
                    if(afterFill)
                    {
                        GameObject.FindObjectOfType<GoalManager>().i += 1;
                    }
                    

                }

            }
            else if (temperature)
            {
                if (temp == 99 && time > 0)
                {
                    time = 0;
                    temp = 100;
                    text.text = temp.ToString() + "°C";
                    temperature = !temperature;
                    lastTemp = true;

                }
            }
            else
            {
                if (temp == 199 && time > 0)
                {
                    time = 0;
                    temp = 200;
                    text.text = temp.ToString() + "°C";
                    tempertureOverride = !tempertureOverride;

                }
                else if (temp > 120 && timer < 0)
                {
                    timer = 5f;
                    GameObject.Find("BackEnd").GetComponent<PlayerEventsStorage>().AddErrs();
                }

            }

        }
        else if(lastTemp)
        {
            if(timerForWater ==10f) controller.ChangeKranState();
            timerForWater -= Time.deltaTime;
            Debug.Log("Timer now is:" + timerForWater);
            if(timerForWater<0)
            {
                controller.ChangeKranState();
                tempertureOverride = true;
                lastTemp = false;
                afterFill = true;
            }
        }
    }

    public void ReduseTemp()
    {
        cur_temp = temp;
        temperature = false;
        tempertureOverride = false;
        temperatureRewind = true;
    }
    public void StartHeating()
    {
        temperature = true;
        tempertureOverride = false;
        temperatureRewind = false;
    }

   
}
