﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class openContainer : MonoBehaviour
{
    public bool isOpen;
    public GameObject door;
    public bool leftDoor;
    public Vector3 colliderPos = new Vector3(0, 0.1f, 0);
    public Vector3 pos = new Vector3(0, 0, 0.1f);
    public Vector3 startPos;
    private void Update()
    {
        if (GetComponent<Interactable>().isHovering == true)
        {
            if (SteamVR_Actions.default_GrabPinch.GetStateDown(SteamVR_Input_Sources.Any))
            {
                isOpen = !isOpen;
            }
        }
        if (isOpen)
        {
            if (leftDoor)
            {
                if (door)
                {
                    door.transform.localPosition = -colliderPos;

                }
                transform.localPosition = pos;
            }
            else
            {
                if (door)
                {
                    door.transform.localPosition = colliderPos;
                }
                transform.localPosition = -pos;
            }
        }
        else
        {
            if (door)
            {

                door.transform.localPosition = new Vector3(0, 0, 0);
                transform.localPosition = new Vector3(0, 0, 0);
            }
            else
            {
                transform.localPosition = startPos;
            }
        }
    }
}
