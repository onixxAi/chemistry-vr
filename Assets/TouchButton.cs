﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class TouchButton : MonoBehaviour
{
    public bool isTouching;
    public AudioClip buttonSFX;
    public Charging charging;
    public bool dron;
    
    private void OnTriggerEnter(Collider other)
    {
        if (dron)
        {
            charging.isCharging = true;
            GetComponent<Collider>().enabled = false;
        }
        else
        {

            GameObject.Find("AudioManager").GetComponent<AudioSource>().PlayOneShot(buttonSFX);
            GameObject.FindObjectOfType<GoalManager>().i += 1;
            Destroy(gameObject);
            isTouching = true;
        }

    }
}
