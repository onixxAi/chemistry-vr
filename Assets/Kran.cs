﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class Kran : MonoBehaviour
{
    public GameObject WaterParticle;
    public Transform SpawnPoint;
    private float timeSpawn = 0.2f;
    public GameObject Hologram;
    public bool waterOk;
    public bool isDisl;

    private void Update()
    {
        if (waterOk)
        {
            timeSpawn -= Time.deltaTime;
            if (timeSpawn <= 0)
            {
                timeSpawn = 0.02f;
                Instantiate(WaterParticle, SpawnPoint);
                if (Hologram)
                {

                    Hologram.SetActive(false);
                }
            }

        }
        if (!isDisl)
        {


            if (GetComponent<Interactable>().isHovering == true)
            {
                if (SteamVR_Actions.default_GrabPinch.GetStateDown(SteamVR_Input_Sources.Any))
                {

                    waterOk = !waterOk;
                }
            }
        }
    }
    public void OpenWater()
    {
        waterOk = !waterOk;
    }
}
