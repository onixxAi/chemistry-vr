﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HandTimer : MonoBehaviour
{
    float time = 1;
    public int seconds;
    public int minutes = 5;
    public bool timerON;
    public bool firstmission;

    private void Update()
    {
        if (timerON)
        {
            GetComponent<TextMeshPro>().color = Color.red;
            gameObject.SetActive(true);
            time -= Time.deltaTime;
            if (time <= 0)
            {
                time = 1;
                if (minutes != 0 || seconds != 0)
                {
                    seconds -= 1;
                }
                if (seconds < 0)
                {
                    if (minutes > 0)
                    {
                        seconds = 59;
                        minutes -= 1;
                    }
                }
            }
            if (seconds >= 10)
            {
                GetComponent<TextMeshPro>().text = minutes + ":" + seconds;
            }
            else
            {
                GetComponent<TextMeshPro>().text = minutes + ":" + "0" + seconds;
            }
            if (minutes == 0 && seconds <= 30)
            {
                GetComponent<TextMeshPro>().color = new Color(GetComponent<TextMeshPro>().color.r, GetComponent<TextMeshPro>().color.g, GetComponent<TextMeshPro>().color.b, Mathf.PingPong(Time.time * 2, 1) - 0f);
            }
            if(minutes <= 0)
            {
                timerON = false;
                if (firstmission)
                {
                    GameObject.FindObjectOfType<GoalManager>().i = GameObject.FindObjectOfType<GoalManager>().checkPointIndex;
                }
                else
                {
                    //to do
                }
            }
        }
        else
        {
            //gameObject.SetActive(false);
            GetComponent<TextMeshPro>().color = Color.green;
        }

    }

}
