﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DozatorControlling : MonoBehaviour
{
    float angle;
    public GameObject waterParticle;
    public Transform waterSpawnPoint;
    public Material colorLiquid;
    public Color blobColor;
    public float timeSpawn = 0.1f;
    void Update()
    {
        timeSpawn -= Time.deltaTime;
        angle = Mathf.Abs(Vector3.SignedAngle(Vector3.up, gameObject.transform.up, Vector3.up));
        if (timeSpawn < 0)
        {
            timeSpawn = 0.1f;
            if (angle > 90)
            {
                GameObject blob = Instantiate(waterParticle, waterSpawnPoint);
                blob.GetComponent<BlobControlling>().liquidMaterial = colorLiquid;
                blob.GetComponent<BlobControlling>().fillAmount = 0.1f;
                blob.GetComponent<ParticleSystem>().startColor = blobColor;
            }

        }
    }
}
