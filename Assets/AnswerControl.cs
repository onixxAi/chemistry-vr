﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AnswerControl : MonoBehaviour
{
    TextMeshPro text;
    public int maxSimbols;
    public bool filled = false;
    public bool readyToGoDown = true;
    private void Start()
    {
        text = GetComponent<TextMeshPro>();
    }
    
    public void WriteSymbol(string c)
    {
        if(c =="Backspace")
        {
            if(text.text.Length>0 && text.text != "__")
            {
                
                text.text = text.text.Remove(text.text.Length - 1, 1);
                if (text.text.Length == 0) text.text = "__";
            }
        }
        else
        {
            if (text.text == "__") text.text = "";
            if(!filled) text.text += c;
            
        }
        filled = text.text.Length == maxSimbols && text.text != "__" ? true : false;
        readyToGoDown = text.text == "__" ? true : false;
    }
    public string GetText()
    {
        return text.text;
    }
    public void Clear()
    {
        filled = false;
        text.text = "__";
        readyToGoDown = true;
    }
}
