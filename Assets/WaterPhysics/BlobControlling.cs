﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlobControlling : MonoBehaviour
{
    public float fillAmount;
    public GameObject hitParticle;
    public float destroyTime = 0.2f;
    public Material liquidMaterial;
    public float liquidMinFill, liquidMaxFill;
    public float liquidMinXYFill, liquidMaxXYFill;
    public bool metil;
    public bool lakmus;
    public bool fenol;




    private void OnParticleCollision(GameObject other)
    {
        if (other.GetComponentInChildren<WaterControlling>())
        {
            
            Debug.Log("WaterOk");
            //float difference = ((liquidMinFill - liquidMaxFill) * Mathf.Pow(liquidMinXYFill - liquidMaxXYFill, 2)) / ((other.GetComponent<WaterControlling>().startMinFill - other.GetComponent<WaterControlling>().startMaxFill) * Mathf.Pow(other.GetComponent<WaterControlling>().minXYFill - other.GetComponent<WaterControlling>().maxXYFill, 2));
            //fillAmount *= difference;
            
            if (other.GetComponentInChildren<WaterControlling>().fillAmount < 1)
            {
                other.GetComponentInChildren<WaterControlling>().FlaskFilling(fillAmount);
                other.GetComponentInChildren<WaterControlling>().ChangeMaterial(liquidMaterial, fillAmount);

            }

            else
            {
                hitParticle = Instantiate(hitParticle, other.GetComponent<WaterControlling>().waterSpawnPoint);
                Destroy(hitParticle, destroyTime);
                Destroy(gameObject, destroyTime + .1f);
            }
        }
        if (other.GetComponent<PadlaControlling>())
        {
            if (liquidMaterial)
            {
                other.GetComponent<PadlaControlling>().ChangeMaterial(liquidMaterial, fillAmount);
                if (metil)
                {
                    other.GetComponent<PadlaControlling>().metil = true;
                }
                if (lakmus)
                {

                    other.GetComponent<PadlaControlling>().lakmus = true;
                }
                if (fenol)
                {
                    other.GetComponent<PadlaControlling>().fenol = true;

                }
                
            }
        }
        if (other.GetComponent<Wtered>())
        {
            Debug.Log("BlobWtered");
            GameObject.FindObjectOfType<HeaterController>().isWtered = true;
        }
        Destroy(gameObject);
    }
}
