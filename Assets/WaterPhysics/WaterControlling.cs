﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterControlling : MonoBehaviour
{
    public Transform waterSpawnPoint;
    public ParticleSystem waterParticle;
    Renderer rend;
    public GameObject flask;
    [HideInInspector]
    public Material startMaterial, endMaterial;
    Vector3 lastPos;
    Vector3 velocity;
    Vector3 lastRot;
    Vector3 angularVelocity;
    public float MaxWobble = 0.03f;
    public float WobbleSpeed = 1f;
    public float Recovery = 1f;
    float wobbleAmountX;
    float wobbleAmountZ;
    float wobbleAmountToAddX;
    float wobbleAmountToAddZ;
    float pulse;
    float time = 0.5f;
    public float minZFill, maxZFill, minXYFill, maxXYFill;

    public float startMinFill, startMaxFill;
    [Range(-1f, 1f)]
    public float fillAmount;
    float timeSpawn;
    public float frequencySpawn;
    float angle = 0.1f;
    [Range(0.1f, 4f)]
    public float miscibility = 1f;
    public Vector3 vec3;

    // Use this for initialization
    void Start()
    {

        startMaterial = GetComponent<Renderer>().material;
        startMaxFill = maxZFill;
        startMinFill = minZFill;
        rend = GetComponent<Renderer>();
    }
    private void Update()
    {
        print(angle);
        angle = Mathf.Abs(Vector3.SignedAngle(Vector3.up, flask.transform.forward, Vector3.up));


        time += Time.deltaTime;
        // decrease wobble over time
        wobbleAmountToAddX = Mathf.Lerp(wobbleAmountToAddX, 0, Time.deltaTime * (Recovery));
        wobbleAmountToAddZ = Mathf.Lerp(wobbleAmountToAddZ, 0, Time.deltaTime * (Recovery));

        // make a sine wave of the decreasing wobble
        pulse = 2 * Mathf.PI * WobbleSpeed;
        wobbleAmountX = wobbleAmountToAddX * Mathf.Sin(pulse * time);
        wobbleAmountZ = wobbleAmountToAddZ * Mathf.Sin(pulse * time);

        timeSpawn -= Time.deltaTime;

        //pour water relative to the angle
        if (fillAmount > 0)
        {
            print("angle "+angle);
            if (angle < 90)
            {
                
                print(angle);
                minZFill = Mathf.Lerp(startMinFill, minXYFill, angle / 90f);
                maxZFill = Mathf.Lerp(startMaxFill, maxXYFill, angle / 90f);
                if (fillAmount > 0.5f / (angle / 180) / 1.8f)
                {
                    print(fillAmount > 0.5f * angle / 180);
                    if (timeSpawn < 0)
                    {
                        Instantiate(waterParticle, waterSpawnPoint).GetComponent<BlobControlling>().liquidMaterial = GetComponent<Renderer>().material;
                        waterParticle.GetComponent<BlobControlling>().fillAmount = angle / 180 * .01f * Time.deltaTime;
                        waterParticle.GetComponent<BlobControlling>().liquidMinFill = startMinFill;
                        waterParticle.GetComponent<BlobControlling>().liquidMinXYFill = minXYFill;
                        waterParticle.GetComponent<BlobControlling>().liquidMaxFill = startMaxFill;
                        waterParticle.GetComponent<BlobControlling>().liquidMaxXYFill =maxXYFill;

                        timeSpawn = frequencySpawn / (angle / 180);
                        fillAmount -= angle / 180 * .01f * Time.deltaTime;
                    }
                }
            }
            if (angle > 90)
            {
                minZFill = Mathf.Lerp(minXYFill - minXYFill * 0.6f, startMinFill, angle / 180);
                maxZFill = Mathf.Lerp(maxXYFill + maxXYFill * 0.6f, startMaxFill, angle / 180);

                if (timeSpawn < 0)
                {
                    Instantiate(waterParticle, waterSpawnPoint).GetComponent<BlobControlling>().liquidMaterial = GetComponent<Renderer>().material;
                    waterParticle.GetComponent<BlobControlling>().fillAmount = angle / 180 * 0.3f * Time.deltaTime;
                    waterParticle.GetComponent<BlobControlling>().liquidMinFill = startMinFill;
                    waterParticle.GetComponent<BlobControlling>().liquidMaxFill = startMaxFill;
                    timeSpawn = frequencySpawn / (angle / 180);
                    fillAmount -= angle / 180 * 0.3f * Time.deltaTime;
                }
            }
        }
        else
        {
            minZFill = startMinFill;
            maxZFill = startMaxFill;
        }


        // send it to the shader
        rend.material.SetFloat("_WobbleX", wobbleAmountX);
        rend.material.SetFloat("_WobbleZ", wobbleAmountZ);
        rend.material.SetFloat("_FillAmount", Mathf.Lerp(minZFill, maxZFill, fillAmount));

        // velocity
        velocity = (lastPos - transform.position) / Time.deltaTime;
        angularVelocity = transform.rotation.eulerAngles - lastRot;

        // add clamped velocity to wobble
        wobbleAmountToAddX += Mathf.Clamp((velocity.x + (angularVelocity.z * 0.2f)) * MaxWobble, -MaxWobble, MaxWobble);
        wobbleAmountToAddZ += Mathf.Clamp((velocity.z + (angularVelocity.x * 0.2f)) * MaxWobble, -MaxWobble, MaxWobble);

        // keep last position
        lastPos = transform.position;
        lastRot = transform.rotation.eulerAngles;
    }
    public void FlaskFilling(float FillAmount)
    {
        fillAmount += FillAmount;
    }
    public void ChangeMaterial(Material blobMaterial, float lerp)
    {
        if (blobMaterial != endMaterial)
        {
            endMaterial = blobMaterial;
            startMaterial.Lerp(startMaterial, endMaterial, lerp);
            lerp = 0;

        }
        lerp += lerp * miscibility;
        GetComponent<Renderer>().material.Lerp(startMaterial, endMaterial, lerp);

    }
}