﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class PickUpDetected : MonoBehaviour
{
    public bool rightObject;
    private CheckRightAnswers checkRightAnswers;
    public GameObject sparkle;
    public TextMeshPro cross;
    public GameObject spawnPoint;
    private void Start()
    {
        checkRightAnswers = GameObject.FindObjectOfType<CheckRightAnswers>();
    }
    private void Update()
    {
        if (GetComponent<Interactable>())
        {
            if (GetComponent<Interactable>().isHovering == true)
            {
                if (SteamVR_Actions.default_GrabPinch.GetStateDown(SteamVR_Input_Sources.Any))
                {
                    if (rightObject)
                    {
                        if (!spawnPoint)
                        {
                            Instantiate(sparkle, transform.position, Quaternion.Euler(90, 0, 0));

                        }
                        else
                        {
                            Instantiate(sparkle, spawnPoint.transform.position, Quaternion.Euler(90, 0, 0));
                        }
                        checkRightAnswers.rightAnswers += 1;
                        gameObject.SetActive(false);
                    }
                    else
                    {
                        Debug.Log("Wrong Answer");
                        cross.text = "X";
                        GameObject.FindObjectOfType<PlayerEventsStorage>().AddErrs();
                    }
                }
            }

        }
        

    }
}
