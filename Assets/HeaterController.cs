﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Valve.VR.InteractionSystem;
using Valve.VR;
using Valve;
public class HeaterController : MonoBehaviour
{

    private ChangeColorButton changerColor;
    private bool buttonPressed = false;
    public TemperatureControl temperature;
    public TextMeshPro textOnButton;
    public Kran kran;
    public bool isReady;
    public bool isWtered;

    // Start is called before the first frame update
    void Start()
    {
        changerColor = GetComponent<ChangeColorButton>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Interactable>().isHovering == true)
        {
            if (SteamVR_Actions.default_GrabPinch.GetStateDown(SteamVR_Input_Sources.Any))
            {
                Debug.Log("Pressed");
                ChangeStateOfButton();
            }
        }
    }
    private void ChangeStateOfButton()
    {
        if (isReady && isWtered)
        {
            buttonPressed = !buttonPressed;
            changerColor.SetPushed(buttonPressed);
            textOnButton.text = buttonPressed ? "ON" : "OFF";
            if (!buttonPressed)
            {
                temperature.ReduseTemp();
            }
            else temperature.StartHeating();
        }
    }
    public void ChangeKranState()
    {
        kran.OpenWater();
    }
}
