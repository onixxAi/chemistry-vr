﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class CapAnimationController : MonoBehaviour
{
    public Animator anim;
    public bool isOpen;
    int close = Animator.StringToHash("Close");
    int open = Animator.StringToHash("Test");
    private void Update()
    {
        if (GetComponent<Interactable>())
        {
            if (GetComponent<Interactable>().isHovering == true)
            {
                if (SteamVR_Actions.default_GrabPinch.GetStateDown(SteamVR_Input_Sources.Any))
                {
                    if (isOpen)
                    {
                        // anim.SetBool("isOpen", false);
                        anim.SetTrigger(close);
                        isOpen = !isOpen;
                    }
                    else
                    {
                        //anim.SetBool("isOpen", true);
                        anim.SetTrigger(open);
                        isOpen = !isOpen;
                    }

                }
            }
        }
    }
}
