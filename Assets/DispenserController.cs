﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class DispenserController : MonoBehaviour
{
    public Animator anim;
    private int open = Animator.StringToHash("Open");
    private int close = Animator.StringToHash("Close");
    private int test = Animator.StringToHash("Test");
    bool isOpen = false;
    public bool isClear = true;
    private float timer = 5f;
    public bool ButtonPusshed = false;
    public GameObject element;
    public Transform spaenPoint;
    private GameObject lastElement;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(isOpen && isClear)
        {
            timer -= Time.deltaTime;
            if (timer < 0)
            {
                ChangeDoor();
                timer = 5f;
                isOpen = !isOpen;
                
            }
        }
        if (GetComponent<Interactable>().isHovering == true)
        {
            if (SteamVR_Actions.default_GrabPinch.GetStateDown(SteamVR_Input_Sources.Any))
            {
                
                ChangeDoor();
                isOpen = !isOpen;
            }
        }
    }
    private void ChangeDoor()
    {
        if(isOpen)
        {
            anim.SetTrigger(close);
        }
        else anim.SetTrigger(test);
    }

    public void SpawnElement(string name, Color color, GameObject oobject)
    {
        
        GameObject instanse = Instantiate(element, spaenPoint);
        instanse.name = name;
        instanse.GetComponent<Renderer>().material.color = color;
        isClear = false;
        lastElement = oobject;

    }

    public void OFFShit()
    {
        lastElement.GetComponent<ButtonElementChooser>().Push();
        lastElement = null;
    }
}
