﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugRotation : MonoBehaviour
{
    public Transform target;

    void Update()
    {
        Vector3 targetDir = target.position - transform.position;
        Vector3 forward = transform.forward;
        //if(Mathf.Abs()
        float angle = Vector3.SignedAngle(Vector3.up, transform.up, Vector3.up);
        //float angle = Vector3.SignedAngle(Vector3.up, forward, Vector3.up)-90;
        //print(angle);
        if (angle < -5.0F)
            print("turn left");
        else if (angle > 5.0F)
            print("turn right");
        else
            print("forward");
    }
}