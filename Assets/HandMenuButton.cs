﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class HandMenuButton : MonoBehaviour
{
    public bool isOpenMenuButton;
    public bool isHitsButton;
    public bool isCheckpointButton;
    public bool isSubtitleTextOff;


    [DrawIf("isOpenMenuButton", true)]
    public bool isOpen;
    [DrawIf("isOpenMenuButton", true)]
    public GameObject openMenuButton;
    [DrawIf("isOpenMenuButton", true)]
    public GameObject handMenuPanel;
    [DrawIf("isHitsButton", true)]
    public GameObject hintSubtitle;
    [DrawIf("isSubtitleTextOff", true)]
    public bool off;

    private void Update()
    {
        
        if (GetComponent<Interactable>().isHovering == true)
        {
            if (SteamVR_Actions.default_GrabPinch.GetStateDown(SteamVR_Input_Sources.Any))
            {
                GetComponent<Interactable>().isHovering = false;
                if (isOpenMenuButton)
                {
                    Kek();
                }
                if (isHitsButton)
                {
                    if (hintSubtitle)
                    {
                        if (!GameObject.FindGameObjectWithTag("Sub"))
                        {
                            GameObject.FindObjectOfType<PlayerEventsStorage>().AddHints();
                            Instantiate(hintSubtitle, GameObject.Find("SubtitlesSpawnPoint").transform);

                        }
                    }
                }
                if (isCheckpointButton)
                {
                    GameObject.FindObjectOfType<GoalManager>().i = GameObject.FindObjectOfType<GoalManager>().checkPointIndex;
                }
                if (isSubtitleTextOff)
                {
                    off = !off;
                }
            }
            //GetComponent<Interactable>().isHovering = false;
        }

    }
    public void Kek()
    {
        if (isOpenMenuButton)
        {

            if (isOpen)
            {
                handMenuPanel.SetActive(true);
                openMenuButton.SetActive(false);
            }
            else
            {
                openMenuButton.SetActive(true);
                handMenuPanel.SetActive(false);
            }
        }
    }
}
