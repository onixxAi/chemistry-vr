﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FinishedCanvas : MonoBehaviour
{

    public TextMeshProUGUI fullNameText;
    public TextMeshProUGUI timeText;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI hintsText;
    private void Update()
    {
        hintsText.text = GameObject.FindObjectOfType<hg.ApiWebKit.apis.example.media.Controlpoint>().hintsSumm.ToString();
    }

}
