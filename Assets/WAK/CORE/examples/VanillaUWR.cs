﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class VanillaUWR : MonoBehaviour 
{
	#if UNITY_2017_3_OR_NEWER
	public enum TEST
	{
		ONE,
		TWO
	}

	public TEST TestNumber = TEST.ONE;

	void Start()
    {
    	switch(TestNumber)
    	{
    		case TEST.ONE:
				StartCoroutine(ONE());
    			break;

			case TEST.TWO:
				StartCoroutine(TWO());
    			break;
    	}
    }

	string _uri = "http://wak-api.unity3dassets.com:8080/exist/restxq/WAK/v5/cars/honda";

	void outputUwrResponse(UnityWebRequest uwr)
	{
		if (uwr.isNetworkError || uwr.isHttpError)
	    {
	        Debug.Log(uwr.error);
	    }
	    else
	    {
	        // Show results as text
	        Debug.Log(uwr.downloadHandler.text);

	        // Or retrieve results as binary data
	        byte[] results = uwr.downloadHandler.data;
	    }
	}

    IEnumerator ONE()
    {
 		using (UnityWebRequest uwr = UnityWebRequest.Get(_uri))
        {
            yield return uwr.SendWebRequest();

            outputUwrResponse(uwr);
        }
    }

    IEnumerator TWO()
    {
		UnityWebRequest uwr = new UnityWebRequest(_uri,"GET");
		uwr.timeout = 10;
		uwr.disposeUploadHandlerOnDispose = true;
		uwr.disposeDownloadHandlerOnDispose = true;
		uwr.downloadHandler = new DownloadHandlerBuffer();
		//uwr.uploadHandler = new UploadHandlerRaw(new byte[] { 0x0 });
		uwr.uploadHandler = new UploadHandlerRaw(null);

		yield return uwr.SendWebRequest();

		outputUwrResponse(uwr);

		uwr.Dispose();
    }
    #endif
}
