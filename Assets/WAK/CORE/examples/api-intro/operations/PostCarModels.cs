using UnityEngine;
using System;

using hg.ApiWebKit.core.http;
using hg.ApiWebKit.core.attributes;
using hg.ApiWebKit.mappers;
using hg.ApiWebKit.faulters;

namespace hg.ApiWebKit.apis.example.apiintro.operations
{
	[HttpPOST]
	[HttpPath("hg.example","/WAK/v1/cars")]
	[HttpTimeout(10f)]
	[HttpProvider(typeof(hg.ApiWebKit.providers.HttpUnityWebRequestClient))]
	//[HttpProvider(typeof(hg.ApiWebKit.providers.HttpUniWebClient))]
	//[HttpProvider(typeof(hg.ApiWebKit.providers.HttpBestHttpClient))]
	//[HttpProvider(typeof(hg.ApiWebKit.providers.HttpClaytonIndustriesClient))]
	//[HttpProvider(typeof(hg.ApiWebKit.providers.HttpUnityHTTPClient))]
	[HttpContentType("application/json")]
	[HttpAccept("application/json")]
	public class PostCarModels: HttpOperation
	{
		[HttpResponseJsonBody]
		public Added Response;

		[HttpRequestJsonBody]
		public Car Request;

		public class Car {
			public string make;
			public string model;
		}

		public class Added {
			public string added;
		}

		protected override void FromResponse (HttpResponse response)
		{
			base.FromResponse (response);

			Debug.Log("added timestamp: "  + Response.added);
		}
	}
}