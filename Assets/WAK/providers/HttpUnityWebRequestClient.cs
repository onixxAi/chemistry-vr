#if UNITY_5_4_OR_NEWER
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using hg.ApiWebKit.core.http;
using UnityEngine.Networking;

namespace hg.ApiWebKit.providers
{
	public class HttpUnityWebRequestClient : HttpAbstractProvider
	{
        private class AcceptAllCerts: CertificateHandler
        {
            protected override bool ValidateCertificate(byte[] certificateData)
            {
                return true;
            }
        }

		protected override IEnumerator sendImplementation ()
		{
			_uwr = new UnityWebRequest(Request.RequestModelResult.Uri,Request.RequestModelResult.Verb);
            _uwr.certificateHandler = new AcceptAllCerts();
			_uwr.chunkedTransfer = false;
			_uwr.timeout = (int)Request.RequestModelResult.Timeout;
			_uwr.disposeUploadHandlerOnDispose = true;
			_uwr.disposeDownloadHandlerOnDispose = true;
			_uwr.downloadHandler = new DownloadHandlerBuffer();

			if(Request.RequestModelResult.Data != null && Request.RequestModelResult.Data.Length > 0)
				_uwr.uploadHandler = new UploadHandlerRaw(Request.RequestModelResult.Data);

			foreach(KeyValuePair<string,string> de in Request.RequestModelResult.Headers)
			{
				_uwr.SetRequestHeader(de.Key,de.Value);
			}

			#if UNITY_2017_2_OR_NEWER
			_uwr.SendWebRequest();
			#else
			_uwr.Send();
			#endif
		
			//_async = _uwr.SendWebRequest();
			//_async.completed += _async_completed;

			while(TimeElapsed <= Request.RequestModelResult.Timeout && !_uwr.isDone && !RequestCancelFlag)
			{
				UpdateTransferProgress();
				yield return null;
			}

			UpdateTransferProgress();

			if(!_uwr.isDone) _uwr.Abort();

			if(RequestCancelFlag)
			{
				RequestCancelFlag = false;
				ChangeState(HttpRequestState.CANCELLED);
			}
			else if(TimeElapsed > Request.RequestModelResult.Timeout)
			{
				ChangeState(HttpRequestState.TIMEOUT);
			}
			else
			{	
				ChangeState(
					string.IsNullOrEmpty(getError())
						? HttpRequestState.COMPLETED
						: HttpRequestState.ERROR);	
			}
			
			BehaviorComplete();
			
			Cleanup();
			
			yield break;
		}

		/*
		void _async_completed (AsyncOperation obj)
		{
			
		}
		*/

		protected override float getTransferProgress ()
		{
			return (_uwr.method==UnityWebRequest.kHttpVerbGET) 
				? _uwr.downloadProgress 
				: _uwr.uploadProgress;
		}

		protected override void disposeInternal()
		{
			if (_uwr != null)
			{
				_uwr.Dispose();
				_uwr = null;
			}
		}

		protected override Dictionary<string,string> getResponseHeaders ()
		{
			return _uwr.GetResponseHeaders();
		}


		protected override string getError ()
		{
			#if UNITY_2017_2_OR_NEWER
			return _uwr.isNetworkError || _uwr.isHttpError
					? _uwr.error
					: null;
			#else
			return _uwr.error;
			#endif
		}


		protected override string getText ()
		{
			return _uwr.downloadHandler==null 
					? null 
					: _uwr.downloadHandler.text;
		}


		protected override byte[] getData ()
		{
			return _uwr.downloadHandler==null 
					? null 
					: _uwr.downloadHandler.data;
		}


		protected override HttpStatusCode getStatusCode()
		{
			return (HttpStatusCode) _uwr.responseCode;
		}


		private UnityWebRequest _uwr = null;
		//private UnityWebRequestAsyncOperation _async = null;
	}
}
#endif