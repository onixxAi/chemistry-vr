﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class ChangeTable : MonoBehaviour
{
   public GameObject a;
    public GameObject b;
    bool text = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (GetComponent<Interactable>().isHovering == true)
        {
            if (SteamVR_Actions.default_GrabPinch.GetStateDown(SteamVR_Input_Sources.Any))
            {
                text = !text;
                if(text)
                {
                    b.SetActive(false);
                    a.SetActive(true);
                }
                else
                {
                    b.SetActive(true);
                    a.SetActive(false);
                }
            }
        }
    }
}
