﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class AlkalineCooking : MonoBehaviour
{
    bool isCook;
    public float cubeWeight;
    public Collider collider;
    public GameObject Subtitles;
    void Update()
    {
        if (cubeWeight == 7)
        {
            GameObject.FindObjectOfType<GoalManager>().i += 1;
            cubeWeight = 0;
            collider.enabled = false;
        }
        else if (cubeWeight >= 7)
        {
            Instantiate(Subtitles, GameObject.Find("SubtitlesSpawnPoint").transform);
            GameObject.FindObjectOfType<PlayerEventsStorage>().AddErrs();
            //sub err
        }
    }
}
