﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
    public string methodName; 
    public GameObject keyboard;
    public GameObject particle;
    public bool missionFinished;
    private void OnTriggerEnter(Collider other)
    {
        if (missionFinished)
        {
            GameObject.FindObjectOfType<GoalManager>().i += 1;
        }
        particle.SetActive(false);
        keyboard.SetActive(true);
    }
    private void OnTriggerExit(Collider other)
    {
        particle.SetActive(true);
        keyboard.SetActive(false);
    }
}
