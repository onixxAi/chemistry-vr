﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateMission : MonoBehaviour
{
    public void UpdateMyMission(GameObject respawnMission,string missionName, Transform missionSpawnPoint)
    {
        Destroy(GameObject.Find(missionName+"(Clone)"));
        Instantiate(respawnMission, missionSpawnPoint);
    }
}
