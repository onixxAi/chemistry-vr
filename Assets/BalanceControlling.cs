﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BalanceControlling : MonoBehaviour
{
    public TextMeshPro tmPro;
    public float weight;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<LiCube>())
        {
            weight += other.GetComponent<LiCube>().weight/2;
            tmPro.text = weight.ToString() + "г";
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<LiCube>())
        {
            weight -= other.GetComponent<LiCube>().weight/2;
            tmPro.text = weight.ToString()+"г";
        }
    }
}
