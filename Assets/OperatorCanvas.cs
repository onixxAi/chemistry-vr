﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class OperatorCanvas : MonoBehaviour
{
    public MainMenu mainMenu;
    private string fullName;
    public int stage;
    public int score;
    public TextMeshProUGUI fullNameText;
    public TextMeshProUGUI timerText;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI stageText;
    public TextMeshProUGUI stageFinishedText;
    public FinishedCanvas finishedCanvas;
    private float time;
    private int seconds;
    private string secondsText;
    private int minutes;
    private string minutesText;

    private void Start()
    {
        fullNameText.text = mainMenu.lastName+" "+ mainMenu.middleName +" "+ mainMenu.firstName;
    }
    private void Update()
    {
        stageFinishedText.text = GameObject.FindObjectOfType<hg.ApiWebKit.apis.example.media.Controlpoint>().curBalls.Count.ToString();
        stageText.text = (10 - GameObject.FindObjectOfType<hg.ApiWebKit.apis.example.media.Controlpoint>().curBalls.Count).ToString();
        scoreText.text = GameObject.FindObjectOfType<hg.ApiWebKit.apis.example.media.Controlpoint>().SummScore().ToString();

        if (stage < 10)
        {
            Timer();
        }
        else
        {
            GetComponent<Canvas>().enabled = false;
            finishedCanvas.GetComponent<Canvas>().enabled = true;
            finishedCanvas.fullNameText.text = fullNameText.text;
            finishedCanvas.timeText.text = timerText.text;
            finishedCanvas.scoreText.text = scoreText.text;

        }
    }
    void Timer()
    {
        time -= Time.deltaTime;

        if (time <= 0)
        {
            if (seconds < 59)
            {
                seconds += 1;
            }
            else
            {
                seconds = 0;
                minutes += 1;

            }
            time = 1;
        }
        if (seconds < 10)
        {
            secondsText = "0" + seconds;
        }
        else
        {
            secondsText = seconds.ToString();
        }

        if (minutes < 10)
        {
            minutesText = "0" + minutes;
        }
        else
        {
            minutesText = minutes.ToString();
        }
        timerText.text = minutesText + ":" + secondsText;
    }
}
